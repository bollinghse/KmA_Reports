## KMA Reports

C'est un projet de site de raports relatif aux tournois de Krosmaster Arena (jeu édité par Ankama).
C'est aussi un projet d'application de compétences de base en Python 3 et de découverte du framework Django.
Au fil du temps, il est évidemment apparu que le javascript et JQuery, ainsi que les CSS étaient indispensables.

Le résultat est en version Béta, un produit qui est en production, qui fonctionne, qui va évoluer, mais qui doit surtout être complètement refactorisé sur ces 4 domaines.

Toute aide est la bienvenue :-p

![Licence AGPL3](agplv3.png)
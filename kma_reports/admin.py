from django.contrib import admin
from kma_reports.models import Site, Joueur, CommunauteJoueurs, Krosmaster, EquipeK, Format, Type, Tournoi, EquipeJ, Ronde, Match, Carte, Pays


# Register your models here.

class SitesAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['nom']}),
        ('Infos Géographiques', {'fields': ['adresse', 'code_postal', 'ville', 'etat', 'pays']}),
        ('Infos Internet', {'fields': ['courriel', 'web', 'facebook', 'twitter', 'logo']}),
    ]


class CommunautesAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['nom']}),
        ('Infos Internet', {'fields': ['courriel', 'web', 'facebook', 'twitter', 'logo']}),
    ]


class JoueursAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Infos Personelles', {'fields': ['nom', 'prenom']}),
        ('Krosmaster Arena', {'fields': ['numero_de_carte', 'displayname', 'pseudo']}),
        ('Encodeur', {'fields': ['compte']})
    ]
    list_display = ('nom', 'prenom', 'pseudo')


admin.site.register(Site, SitesAdmin)
admin.site.register(Joueur, JoueursAdmin)
admin.site.register(EquipeJ)
admin.site.register(CommunauteJoueurs, CommunautesAdmin)
admin.site.register(Krosmaster)
admin.site.register(EquipeK)
admin.site.register(Format)
admin.site.register(Type)
admin.site.register(Tournoi)
admin.site.register(Ronde)
admin.site.register(Match)
admin.site.register(Carte)
admin.site.register(Pays)

from django import forms
from django.contrib.auth import authenticate
from django.db.models.functions import Coalesce
from django.utils.translation import ugettext_lazy as _
from kma_reports.models import Carte, Pays, Site, CommunauteJoueurs, Joueur, Tournoi, Type, KrosmasterSet


class UserForm(forms.Form):

    username = forms.EmailField(
        label=_('Courriel')
    )
    password = forms.CharField(
        label=_('Mot de passe'),
        widget=forms.PasswordInput
    )

    def clean(self):
        # Redéfini la méthode clean en appelant la version de la classe parent à l'aide de "super"
        cleaned_data = super(UserForm, self).clean()
        # récupère la valeur nettoyée des champs (vides si non valides)
        username = cleaned_data.get("username")
        password = cleaned_data.get("password")
        if username and password:
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    return cleaned_data
                else:
                    raise forms.ValidationError(_('Le mot de passe est valide, mais le compte est désactivé !'))
            else:
                raise forms.ValidationError(_('Compte ou mot de passe incorrect !'))


class ProfileForm(forms.Form):
    first_name = forms.CharField(
        widget=forms.TextInput,
        required=False,
        label=_('Prénom')
    )
    last_name = forms.CharField(
        widget=forms.TextInput,
        required=False,
        label=_('Nom')
    )
    email = forms.EmailField(
        widget=forms.TextInput,
        required=True,
        label=_("Courriel comme Nom d'utilisateur"),
    )


class MdpForm(forms.Form):

    mdp0 = forms.CharField(
        label=_('Mot de passe') + ' :',
        required=True,
        widget=forms.PasswordInput(
            attrs={'placeholder': _('Actuel')})
    )
    mdp1 = forms.CharField(
        label=_('Mot de passe') + ' :',
        required=True,
        widget=forms.PasswordInput(
            attrs={'placeholder': _('Nouveau')})
    )
    mdp2 = forms.CharField(
        label=_('Mot de passe') + ' :',
        required=True,
        widget=forms.PasswordInput(
            attrs={'placeholder': _('Vérification')})
    )


class Recherche(forms.Form):

    lieu = forms.CharField(
        widget=forms.TextInput(
            attrs={'id': 'LieuSelection', 'type': 'text'}),
        required=False,
        label=''
    )
    organisateur = forms.CharField(
        widget=forms.HiddenInput(
            attrs={'id': 'organisateur', 'type': 'text'}),
        required=False,
        label=_('Organisateur')
    )
    coorg = forms.CharField(
        widget=forms.TextInput(
            attrs={'id': 'CoorgSelection', 'type': 'text'}),
        required=False,
        label=''
    )
    coorganisateur = forms.CharField(
        widget=forms.HiddenInput(
            attrs={'id': 'coorganisateur', 'type': 'text'}),
        required=False,
        label=_('Co-organisateur')
    )


class Recherche_J(forms.Form):
    joueur = forms.ModelChoiceField(
        queryset=Joueur.objects.all().annotate(presentation=Coalesce('pseudo', 'displayname')).order_by('presentation'),
        widget=forms.Select(
            attrs={'id': 'joueur'}),
        required=False,
        label=_('Joueurs')
    )


class Recherche_T(forms.Form):
    mois_val = (
        ('0', _('Mois')),
    )
    annee = forms.ModelChoiceField(
        # flat=True -> retourne seulement la valeur et non <'annee': 2016> dans la liste du formulaire de choix
        queryset=Tournoi.objects.values_list('annee', flat=True).distinct(),
        empty_label=_('Année'),
        widget=forms.Select(
            attrs={'id': 'annee'}),
        required=False,
        label=_('Année')
    )
    mois = forms.ChoiceField(
        choices=mois_val,
        widget=forms.Select(
            attrs={'id': 'mois', 'disabled': True}),
        required=False,
        label=_('Mois')
    )
    pays = forms.ModelChoiceField(
        # flat=True -> retourne seulement la valeur et non <'annee': 2016> dans la liste du formulaire de choix
        queryset=Site.objects.values_list('pays__nom', flat=True).distinct(),
        empty_label=_('Pays'),
        widget=forms.Select(
            attrs={'id': 'pays'}),
        required=False,
        label=_('Pays')
    )
    site = forms.ModelChoiceField(
        queryset=Site.objects.all().order_by('nom'),
        empty_label=_('Organisateur'),
        widget=forms.Select(
            attrs={'id': 'organisateur'}),
        required=False,
        label=_('Organisateur')
    )
    co_organisateur = forms.ModelChoiceField(
        queryset=CommunauteJoueurs.objects.all().order_by('nom'),
        empty_label=_('Communauté'),
        widget=forms.Select(
            attrs={'id': 'coorganisateur'}),
        required=False,
        label=_('Communauté de joueurs')
    )
    carte = forms.ModelChoiceField(
        queryset=Carte.objects.filter(id__in=Tournoi.objects.values('carte_id').distinct()),
        empty_label=_('Carte'),
        widget=forms.Select(
            attrs={'id': 'carte'}),
        required=False,
        label=_('Carte')
    )
    type = forms.ModelChoiceField(
        queryset=Type.objects.filter(id__in=Tournoi.objects.values('type_id').distinct()),
        empty_label=_('Type'),
        widget=forms.Select(
            attrs={'id': 'type'}),
        required=False,
        label=_('Type'),
        localize=True
    )


class DocumentForm(forms.Form):

    docfile = forms.FileField(
        widget=forms.FileInput(
            attrs={'id': 'awrxSelection'}),
        label=_('Fichier .awrx')
    )
    krosmasterset = forms.ModelChoiceField(
        queryset=KrosmasterSet.objects.all().exclude(nom="indéfini").order_by('-id'),  # - pour ordre inverse
        widget=forms.RadioSelect(
            attrs={'id': 'krosmasterset'}),
        label=_(''),
        empty_label=None
    )
    carte = forms.ModelChoiceField(
        queryset=Carte.objects.all().order_by('nom'),
        widget=forms.Select(
            attrs={'id': 'carte'}),
        label=_('Carte')
    )
    rotation = forms.BooleanField(
        widget=forms.CheckboxInput(
            attrs={'id': 'rotation'}),
        required=False,
        label=_("Rotation (Cochez si la carte a été pivotée d'1/4 de tour)")
    )
    lieu = forms.CharField(
        widget=forms.TextInput(
            attrs={'id': 'LieuSelection', 'type': 'text'}),
        required=False,
        label=_('Lieu')
    )
    organisateur = forms.CharField(
        widget=forms.HiddenInput(
            attrs={'id': 'organisateur', 'type': 'text'}),
        required=False,
        label=_('Organisateur')
    )
    coorg = forms.CharField(
        widget=forms.TextInput(
            attrs={'id': 'CoorgSelection', 'type': 'text'}),
        required=False,
        label=_('Co-organisateur')
    )
    coorganisateur = forms.CharField(
        widget=forms.HiddenInput(
            attrs={'id': 'coorganisateur', 'type': 'text'}),
        required=False,
        label=_('Co-organisateur')
    )


class TournoiForm(forms.Form):
    nom = forms.CharField(
        widget=forms.TextInput(
            attrs={'type': 'text'}),
        label=_('Nom du site organisateur') + ' :'
    )
    date = forms.CharField(
        widget=forms.TextInput(
            attrs={'type': 'text'}),
        required=False
    )
    adresse = forms.CharField(
        widget=forms.TextInput(
            attrs={'type': 'text'}),
        required=False,
        label=_('Rue, Avenue, ... + N°') + ' :'
    )
    code_postal = forms.CharField(
        widget=forms.TextInput(
            attrs={'id': 'encode_tournoi', 'type': 'text'}),
        required=False,
        label=_('Code Postal') + ' :'
    )
    ville = forms.CharField(
        widget=forms.TextInput(
            attrs={'id': 'encode_tournoi', 'type': 'text'}),
        required=False,
        label=_('Ville') + ' :'
    )
    pays = forms.ModelChoiceField(
        queryset=Pays.objects.all().order_by('nom'),
        widget=forms.Select(
            attrs={'id': 'encode_tournoi'}),
        label=_('Pays') + ' :'
    )
    etat = forms.CharField(
        widget=forms.TextInput(
            attrs={'id': 'encode_tournoi', 'type': 'text'}),
        required=False,
        label=_('Etat') + ' :'
    )
    logo = forms.ImageField(
        required=False,
        label=_('Logo') + ' :'
    )
    web = forms.URLField(
        widget=forms.TextInput(
            attrs={'id': 'encode_tournoi', 'type': 'text'}),
        required=False,
        label=_('Site Web') + ' :'
    )
    twitter = forms.URLField(
        widget=forms.TextInput(
            attrs={'id': 'encode_tournoi', 'type': 'text'}),
        required=False,
        label=_('Compte Twitter') + ' :'
    )
    facebook = forms.URLField(
        widget=forms.TextInput(
            attrs={'id': 'encode_tournoi', 'type': 'text'}),
        required=False,
        label=_('Compte Facebook') + ' :'
    )
    courriel = forms.EmailField(
        widget=forms.TextInput(
            attrs={'id': 'encode_tournoi', 'type': 'text'}),
        required=False,
        label=_('Adresse Courriel') + ' :'
    )
    CommunauteJoueurs_nom = forms.CharField(
        widget=forms.TextInput(
            attrs={'id': 'encode_tournoi', 'type': 'text'}),
        required=False,
        label=_('Nom de la communauté') + ' :'
    )
    CommunauteJoueurs_web = forms.URLField(
        widget=forms.TextInput(
            attrs={'id': 'encode_tournoi', 'type': 'text'}),
        required=False,
        label=_('Site Web') + ' :'
    )
    CommunauteJoueurs_twitter = forms.URLField(
        widget=forms.TextInput(
            attrs={'id': 'encode_tournoi', 'type': 'text'}),
        required=False,
        label=_('Compte Twitter') + ' :'
    )
    CommunauteJoueurs_facebook = forms.URLField(
        widget=forms.TextInput(
            attrs={'id': 'encode_tournoi', 'type': 'text'}),
        required=False,
        label=_('Compte Facebook') + ' :'
    )
    CommunauteJoueurs_courriel = forms.EmailField(
        widget=forms.TextInput(
            attrs={'id': 'encode_tournoi', 'type': 'text'}),
        required=False,
        label=_('Adresse Courriel') + ' :'
    )
    CommunauteJoueurs_logo = forms.ImageField(
        required=False,
        label=_('Logo') + ' :'
    )


# class JoueurForm(forms.Form):
#     numero_de_carte = forms.CharField(widget=forms.HiddenInput())
#     prenom = forms.CharField()
#     nom = forms.CharField()
#     displayname = forms.CharField()
#     pseudo = forms.CharField(widget=forms.TextInput(attrs={'type': 'text'}),label='Pseudo :')
#
#
# class KrosmasterForm(NgModelFormMixin, forms.ModelForm):
#     """
#      Krosmasters Form with a little crispy forms added!
#      """
#
#     def __init__(self, *args, **kwargs):
#         super(KrosmasterForm, self).__init__(*args, **kwargs)
#         setup_bootstrap_helpers(self)
#
#     class Meta:
#         model = Krosmaster
#         fields = ('nom', 'niveau', 'initiative', 'boss', 'nombre_max', 'autorise', 'numero',)
#
#     def setup_bootstrap_helpers(object):
#         object.helper = FormHelper()
#         object.helper.form_class = 'form-horizontal'
#         object.helper.label_class = 'col-lg-3'
#         object.helper.field_class = 'col-lg-8'
#
#
# class ContactForm(forms.Form):
#     name = forms.CharField(max_length=30)



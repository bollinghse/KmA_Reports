// variable de langue créée à partir de l'URL
lang = window.location.pathname.substr(0,3);

//  Choix d'un site existant ou laisser vide

$("#LieuSelection").focus();

// TODO trouver une solution pour vider les données si on change d'avis et qu'on supprime le contenu du champs de sélection 

$(function () {
    $("#LieuSelection").autocomplete({
        autoFocus: true, // focus d'office une valeur de la liste
        source: lang + "/kma_reports/api/get_sites/",
        minLength: 2,
        select: function (event, ui) {
            $("#LieuSelection").val(ui.item.label);
            $("#organisateur").val(JSON.stringify(ui.item));
            return false;
        }
    });
});

$(function () {
    $("#CoorgSelection").autocomplete({
        autoFocus: true, // focus d'office une valeur de la liste
        source: lang +"/kma_reports/api/get_communautes/",
        minLength: 2,
        select: function (event, ui) {
            $("#CoorgSelection").val(ui.item.label);
            $("#coorganisateur").val(JSON.stringify(ui.item));
            return false;
        }
    });
});
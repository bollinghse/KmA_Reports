krosmasteraajouter = {};
equipekrosmaster = {
    "kroconteneur_1":{},
    "kroconteneur_2":{},
    "kroconteneur_3":{},
    "kroconteneur_4":{},
    "kroconteneur_5":{},
    "kroconteneur_6":{},
    "kroconteneur_7":{},
    "kroconteneur_8":{}
};
ordreequipe = [
    "kroconteneur_1",
    "kroconteneur_2",
    "kroconteneur_3",
    "kroconteneur_4",
    "kroconteneur_5",
    "kroconteneur_6",
    "kroconteneur_7",
    "kroconteneur_8"
];
nombrefig = 0;
niveautotal = 0;
initiativetotale = 0;
boss = 0;
is_checked_ok = false;
statusdupost = false;

// variable de langue créée à partir de l'URL
lang = window.location.pathname.substr(0,3);

// Lance un premier contrôle
Controles();

//  Gestion du drag and drop + réévaluation
//
$(function () {
    // Ici se gère la sérialisation du drag & drop pour connaitre l'ordre des "kroconteneurs"
    $("#equipe").sortable({
        axis: 'x',
        update: function () {
            var chaine = $(this).sortable("serialize");
            var ordre = chaine.split("&");
            // toilettage pour obtenir une "ordreequipe" propre [ "kroconteneur_2", "kroconteneur_3", ...
            ordre.forEach(
                // passe de kroconteneur=x à kroconteneur_x
                function (element, index) {
                    elementid = element.split("[]=");
                    kroconteneurnom = elementid[0] + '_' + elementid[1];
                    ordreequipe[index] = kroconteneurnom;
                }
            );
            // Vérification des contraintes
            Controles();
        }
    });
});

//  Gestion du menu contextuel: clic droit dans la zone drag & drop
//
$(function () {
    $('#equipe').nuContextMenu({
            hideAfterClick: true,
            items: ['.krosmaster', '.krosmaster_error'],
            callback: function (key, element) {
                if (key === "retirer"){
                    var numid = $(element).parent().parent().attr('id').slice(-1);
                    SupprimerFig(numid);
                }
            },
            menu: [
                { name: 'retirer', title: 'Retirer', icon: 'trash' }
            ]
        });
    $(document).on("contextmenu",function(e){
        if(e.target.nodeName != "#equipe div.krosmaster" || e.target.nodeName != "#equipe div.krosmaster_error")
             e.preventDefault();
     });
});

//  Gestion du formulaire et du boutton +
//
// TODO travailler pour affiner la liste obtenue pour ne plus avoir de boss ou de niv quand plus possible
// TODO trouver une solution pour contraindre les choix dans la liste: pas de valeur libre acceptée
$(function () {
    src = lang + "/kma_reports/api/get_krosmasters/";
    $("#KroSelection").autocomplete({
        autoFocus: true, // focus d'office une valeur de la liste
        source: function (request, response) {
            $.ajax({
                url: src,
                datatype: "json",
                data: {
                    term: request.term,
                    ks_id: ks_id
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            $("#KroSelection").val(ui.item.label);
            krosmasteraajouter = ui.item;
            AjouterFig(krosmasteraajouter);
            krosmasteraajouter = {};
            $("#KroSelection").val(null);
            return false;
        }
    });
});

//  Ajout/Suppression de figurine à l'équipe
//
function AjouterFig(krosobjet) {
    if ($.isEmptyObject(krosobjet) === false) {
        var conteneurdispo = VerifierConteneurDispo();
        var idconteneurdispo = '#' + conteneurdispo;
        // Si la réponse n'est pas vide (faux que vide), il y a de la place et on agit
        if ($.isEmptyObject(conteneurdispo) === false) {
            // partie variables logiques et dictionnaires
            equipekrosmaster[conteneurdispo] = krosobjet;
            nombrefig += 1;
            niveautotal += krosobjet["niveau"];
            initiativetotale += krosobjet["initiative"];
            if (krosobjet["boss"] === true) {
                boss += 1
            }
            // partie conteneur HTML
            $(idconteneurdispo).replaceWith('<div class="krosmaster" id="' + conteneurdispo + '"></div>');
            // On ne prend que les 3 1ers caractères du numéro, car l'image a toujours "a" pour 4ème caractère
            var htmlfig = '<div class="fig"><img src="/static/kma_reports/img/krosmasters/' + krosobjet["numero"].substring(0, 3) + 'a.figurine.png" title="' + krosobjet["label"] + '"></div>';
            $(htmlfig).appendTo($(idconteneurdispo));
            var htmlcaracsdeb = '<div class="caracs">';
            $(htmlcaracsdeb).appendTo($(idconteneurdispo));
            var htmlnivinit = '<div class="niv">' + krosobjet["niveau"] + '</div><div class="init">' + krosobjet["initiative"] + '</div>';
            $(htmlnivinit).appendTo($(idconteneurdispo));
            if (krosobjet["boss"] === true) {
                var htmlboss = '<div class="boss">B</div>';
                $(htmlboss).appendTo($(idconteneurdispo));
            }
            var htmlicones = '<div class="ico_niv"></div><div class="ico_init"></div><div class="ico_boss"></div>';
            $(htmlicones).appendTo($(idconteneurdispo));
            var htmlcaracsfin = '</div>';
            $(htmlcaracsfin).appendTo($(idconteneurdispo));
            // Vérification des contraintes
            Controles();
            // sinon on alerte
        } else {
            alert('Plus de place !')
        }
    } else {
        $("#KroSelection").val(null);
    }
}

function SupprimerFig(num) {
    var conteneur = 'kroconteneur_' + num;
    var div_id = '#' + conteneur;
    var krosobjet = equipekrosmaster[conteneur];
    equipekrosmaster[conteneur] = {};
    nombrefig -= 1;
    niveautotal -= krosobjet["niveau"];
    initiativetotale -= krosobjet["initiative"];
    if (krosobjet["boss"] === true) {boss -= 1}
    $(div_id).replaceWith('<div class="kroconteneur_vide" id="' + conteneur + '"></div>');
    // Vérification des contraintes
    Controles();
}

/**
 * @return {string}
 */
function VerifierConteneurDispo() {
    // regarde dans l'ordre des équipe: ordreequipe
    var conteneur = '';
    ordreequipe.some(
        function (element) {
            // vérifie le contenur dans l'équipe : equipekrosmaster
            var contenu = equipekrosmaster[element];
            if ($.isEmptyObject(contenu) === true) {
                conteneur = element;
                return true
            }
        }
    );
    return conteneur
}

function Controles() {
    // check sont à true (valide) et seront passés à false (non valide) si un problème est tétecté
    var checked = { "check_fig": true, "check_niv": true, "check_init": true, "check_boss": true };

    // Nombre de krosmaster dans l'équipe (Set & Check)
    $("#total_fig").text(nombrefig);
    if (nombrefig < 3) {
        checked["check_fig"] = false;
    }

    // Figurine pas plus présente qu'autorisée
    numeration = {};
    ordreequipe.forEach(function (conteneur) {
        identifiant = "";
        nom = equipekrosmaster[conteneur]['label'];
        if ("numero" in equipekrosmaster[conteneur]) {
            // ajoute à la valeur si clé existe, sinon ajoute 1 à 0, puis affecte
            identifiant = equipekrosmaster[conteneur]["numero"];
            numeration[identifiant] = (numeration[identifiant] || 0) + 1;
        }
        if (numeration[identifiant] > equipekrosmaster[conteneur]['nombre_max']) {
            numid = conteneur.slice(-1);
            alert('Vous ne pouvez pas rajouter plus de: ' + nom);
            SupprimerFig(numid);
        }
    });

    // TODO vérifier qu'une figurine n'est pas plus présente qu'autorisé !
    // Niveau de l'équipe (Set & Check)
    $("#total_niv").text(niveautotal);
    if (niveautotal != 12) {
        checked["check_niv"] = false;
    }

    // Ordre d'Initiative (Set & Check)
    $("#total_init").text(initiativetotale);
    var ldip = []; // Liste Des Initiatives Placées: ordre choisi des figurines
    var ldcrp = []; // Liste Des Conteneurs Remplis Placés: ordre choisi des figurines
    var ldit = []; // Liste Des Initiatives Triées: ordre trié de la plus grande initiative à la plus petite
    ordreequipe.forEach(function (conteneur) {
        if ("initiative" in equipekrosmaster[conteneur]) {
            ldip.push(equipekrosmaster[conteneur]["initiative"]);
            ldcrp.push(conteneur);
            ldit.push(equipekrosmaster[conteneur]["initiative"]);
        }
    });
    // ATTENTION sort est par défaut alphabétique, il faut lui donner une fct de comparaison pour les chiffres
    // si a-b est négatif alors a < que b pour l'ordre. http://www.w3schools.com/jsref/jsref_sort.asp
    ldit.sort(function(a, b){return a-b}).reverse();
    ldip.forEach(function (initiative, index) {
       if (initiative != ldit[index]) { // si cela ne match pas, c'est que l'ordre choisit n'est pas conforme à l'ordre attendu
           $("#" + ldcrp[index]).attr("class", "krosmaster_error");
           console.log("#" + ldcrp[index]);
           checked["check_init"] = false;
       } else {
           $("#" + ldcrp[index]).attr("class", "krosmaster");
       }
    });

    // Boss (Set & Check)
    $("#existe_boss").text(boss);
    if (boss > 1) {
        checked["check_boss"] = false;
    }

    // Application des controles
    for (var clef in checked) {
        if (checked[clef] === false) {
            $("#" + clef).attr("class", "glyphicon glyphicon-remove");
            $("#" + clef).attr("style", "color:red; font-size: 15px");
        } else {
            $("#" + clef).attr("class", "glyphicon glyphicon-ok");
            $("#" + clef).attr("style", "color:limegreen; font-size: 15px");
        }
    }

    // Boutton submit activé, seulement si tous les checks sont true
    is_checked_ok = checked["check_fig"] && checked["check_niv"] && checked["check_init"] && checked["check_boss"];
    if (is_checked_ok === true) {
         slot = [];
        j = 1;
        for (i = 0; i < 8; i++) {
            if ($.isEmptyObject(equipekrosmaster[ordreequipe[i]])) {
            } else {
                slot[j] = equipekrosmaster[ordreequipe[i]];
                j += 1;
            }
        }
        // console.log(statusdupost); // doit être à false ici
        $.ajax({
            type: 'POST',
            url: 'encode_equipes',
            data: {eqk: slot, eqj: eqj},
            success: function(resultData){
                console.log("réponse du post équipe:");
                console.log(resultData);
                //On vérifie si le POST a bien été effectué en vérifiant que l'on a bien récupérer la réponse du post
                if (resultData == "ok") {
                    console.log("gnééé");
                    $("#encoder").attr("disabled", !(is_checked_ok === true));
                    $("#encoder").focus();// si true, le href est validé par un return true, sinon il faut reclicker pour rePOSTer
                } else {
                    alert('Encoding error, please contact Krosympas');
                }
            }
        });

    } else {
        $("#KroSelection").focus();
    }
}

// memo
//------
// {
// label: "Captain Amakna", 
// value: "Captain Amakna",
// autorise: true,
// boss: true,
// numero: "045a", 
// nombre_max: 1,
// initiative: 8,
// niveau: 6,
// }
var csrftoken = Cookies.get('csrftoken');

// variable de langue créée à partir de l'URL
lang = window.location.pathname.substr(0,3);

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

$("#annee").change(
    function () {
        annee = $("#annee").val();
        if (annee) {
            $("#mois").html('');
            $.getJSON(lang + "/kma_reports/api/get_mois/", {term: annee}, function(result) {
                $.each(result, function( key, val) {
                    $("#mois").append('<option value="' + key + '">' + val + '</option>');
                });
            });
            $("#mois").attr("disabled", false);
        } else {
            $("#mois").html('<option value="0">&lt;Mois&gt;</option>');
            $("#mois").attr("disabled", true);
        }
});

$(function () {
    // $("#Krosmaster").autocomplete({
    //     autoFocus: true, // focus d'office une valeur de la liste
    //     source: lang + "/kma_reports/api/get_krosmasters/",
    //     minLength: 2,
    //     select: function (event, ui) {
    //         $("#Krosmasters").val(ui.item.label);
    //         numero = ui.item['numero'];
    //         window.location.href = lang + "/kma_reports/affichage_krosmaster?k=" + numero;
    //         return false;
    //     }
    // });
    src = lang + "/kma_reports/api/get_krosmasters/";
    $("#Krosmaster").autocomplete({
        autoFocus: true, // focus d'office une valeur de la liste
        source: function (request, response) {
            $.ajax({
                url: src,
                datatype: "json",
                data: {
                    term: request.term,
                    autorise: "", // prend la variable autorise du template (eternel, saison, etc.)
                    etoiles: 3 // prend la variable du nombre d'étoile (0 = Saison, 1 à 3 = Eternel)
                    // format : $("#format").val()
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            $("#Krosmasters").val(ui.item.label);
            numero = ui.item['numero'];
            window.location.href = lang + "/kma_reports/affichage_krosmaster?k=" + numero;
            return false;
        }
    });
});

$(function () {
    $("#Joueur").autocomplete({
        autoFocus: true, // focus d'office une valeur de la liste
        source: lang + "/kma_reports/api/get_joueurs/",
        minLength: 2,
        select: function (event, ui) {
            $("#Joueur").val(ui.item.label);
            numero = ui.item['numero'];
            window.location.href = lang + "/kma_reports/affichage_joueur?j=" + numero;
            return false;
        }
    });
});

$(function () {
    $("#Carte").autocomplete({
        autoFocus: true, // focus d'office une valeur de la liste
        source: lang + "/kma_reports/api/get_maps/",
        minLength: 2,
        select: function (event, ui) {
            $("#Carte").val(ui.item.label);
            numero = ui.item['numero'];
            window.location.href = lang + "/kma_reports/affichage_carte?c=" + numero;
            return false;
        }
    });
});

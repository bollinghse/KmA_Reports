import locale
import calendar
import copy
import re
import time
import json
from operator import itemgetter
# from django import forms
import dateutil.parser
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
# from django.core.mail import send_mail
from django.db.models import Q
from django.utils.datastructures import MultiValueDictKeyError
from django.utils.datetime_safe import datetime
from django.utils.translation import ugettext as _
from krosympas import settings
from lxml import etree
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from kma_reports.forms import DocumentForm, TournoiForm, UserForm, ProfileForm, MdpForm, Recherche_T
from kma_reports.models import Joueur, Krosmaster, Site, CommunauteJoueurs, Tournoi, Ronde, EquipeJ, Match, EquipeK, \
    EquipeK_Krosmaster, Carte, Pays, Format, Type, KrosmasterSet
from setuptools.package_index import unique_everseen

# Variable gloable pour lister les Equipes Ayant Réellement Joués (gestion des erreur d'encodage, supprimés dans AWER)
earj = []

def index(request):
    return redirect('/kma_reports/')


def soutien(request):
    return render(request, 'soutien.html')


def infos(request):
    return redirect('/kma_reports/')


def accueil(request):
    #
    if request.method == 'POST':
        print("POST")
        print(request.POST)
        if 'krosnum' in request.POST:
            return HttpResponseRedirect('affichage_krosmaster?k=' + str(request.POST["krosnum"]))
    #     if 'organisateur' in request.POST:
    #         return HttpResponseRedirect('affichage_orga?o=' + str(request.POST["organisateur"]))
    #     elif 'coorganisateur' in request.POST:
    #         return HttpResponseRedirect('affichage_orga?c=' + str(request.POST["coorganisateur"]))
    else:
        tournois = derniers_t(type='general', nombre=3)
        form_t = Recherche_T()
        # form_k = Recherche_K()
        # send_mail(
        #     'Test as Subject',
        #     'Body and content of the mail.',
        #     'no-reply@krosympas.be',
        #     ['sebastien@bollingh.eu'],
        #     fail_silently=False,
        # )
        return render(request, 'accueil.html',
                     {'form_t': form_t,
                     # 'form_k': form_k,
                     'tournois': tournois,
                       })


def auth_login(request):
    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            login(request, user)
            return HttpResponseRedirect('upload_tournoi')
    else:
        form = UserForm()
    return render(request, 'auth_login.html', {'form': form})

@login_required()
def auth_logout(request):
    logout(request)
    tournois = derniers_t(type='general', nombre=3)
    form_t = Recherche_T()
    return HttpResponseRedirect('accueil')


@login_required
def auth_profile(request):
    profile = User.objects.filter(id=request.session['_auth_user_id'])[0]
    dict_profile = {'last_name': profile.last_name,
                    'first_name': profile.first_name,
                    'email': profile.get_username()}
    if request.method == "POST":
        p = request.POST

        if 'email' in p:
            form_profile = ProfileForm(p)
            if form_profile.is_valid():
                f = form_profile.cleaned_data
                update_dict = {}
                for clef in f:
                    if f[clef] != dict_profile[clef]:
                        update_dict[clef] = f[clef]
                if 'email' in update_dict:
                    list_emails = User.objects.filter(email=f['email'])
                    if len(list_emails) > 0:
                        messages.error(request, _("Cette adresse courriel est déjà utilisée") + " !")
                        update_dict.pop("email")
                if len(update_dict) > 0:
                    User.objects.filter(pk=request.session['_auth_user_id']).update(**update_dict)
                    messages.success(request, _("Félicitation, votre profile à été modifié avec succès") + " !")
                else:
                    messages.success(request, _("Pas de modification à apporter") + "...")
            form_mdp = MdpForm()

        elif 'mdp0' in p:
            form_mdp = MdpForm(p)
            if form_mdp.is_valid():
                f = form_mdp.cleaned_data
                if profile.check_password(f['mdp0']):
                    if f['mdp1'] != f['mdp2']:
                        messages.error(request, _("Le nouveau mot de passe et la vérification ne correspondent pas") + " !")
                    else:
                        profile.set_password(f['mdp1'])
                        profile.save()
                        messages.success(request, _("Félicitation, votre mot de passe à été modifié avec succès"))
                        messages.success(request, _("Vous allez être redirigé vers la page de connexion"))
                        return render(request, 'auth_success.html')
                else:
                    messages.error(request, _("Votre mot de passe ACTUEL n'est pas correct") + " !")
            form_profile = ProfileForm(dict_profile)
    else:
        form_profile = ProfileForm(dict_profile)
        form_mdp = MdpForm()
    return render(request, 'auth_profile.html', {'form_profile': form_profile, 'form_mdp': form_mdp, })


@login_required
def upload_tournoi(request):
    print(request.user.username)
    print('UPLOAD IN')
    print(request.method)
    if request.method == 'POST':
        print('UPLOAD POST')
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            if request.POST['organisateur']:
                # transforme le contenu du POST en dictionnaire stocké dans la session
                request.session['organisateur'] = json.loads(request.POST["organisateur"])
                # remplace la clé 'label' par 'nom' et on supprime 'value' qui ne servait que pour autocomplete
                request.session['organisateur']['nom'] = request.session['organisateur'].pop('label')
                request.session['organisateur'].pop('value')
            if request.POST['coorganisateur']:
                # transforme le contenu du POST en dictionnaire stocké dans la session
                request.session['coorganisateur'] = json.loads(request.POST["coorganisateur"])
                # remplace la clé 'label' par 'nom' et on supprime 'value' qui ne servait que pour autocomplete
                request.session['coorganisateur']['nom'] = request.session['coorganisateur'].pop('label')
                request.session['coorganisateur'].pop('value')
            # Analyse du fichier AWER (xml)
            awrx = etree.parse(request.FILES['docfile'])
            carte = {}
            carte['id'] = request.POST["carte"]
            krosmasterset = {'id': request.POST["krosmasterset"]}
            if 'rotation' in request.POST:
                carte['rotation'] = True
            else:
                carte['rotation'] = False
            request.session['tournoi'] = tournoi_awrx(awrx, carte, krosmasterset)
            if 'existe' in request.session['tournoi']:
                return render(request, 'upload_tournoi.html',
                              {'form': form,
                               'erreur': True})
            request.session['matchs'] = matchs_awrx(awrx)
            request.session['joueurs'] = joueurs_awrx(awrx)
            # TODO envisager d'enregistrer le fichier awer, définir la constuction du nom (timestamp_site.awrx ?)
            print('UPLOAD OUT')
            return HttpResponseRedirect('encode_tournoi')
    else:
        print('UPLOAD RESET')
        # Nettoyer les variables de session pour permettre de modifier ses choix avec un retour arrière
        cles_de_session = ['organisateur', 'coorganisateur', 'tournoi', 'joueurs', 'matchs']
        # TODO voir dans le JS pour remettre les valeurs des champs de sélection à zéro ...
        for key in cles_de_session:
            if key in request.session:
                del request.session[key]
        if 'docfile' in request.FILES:
            del request.FILES['docfile']
        form = DocumentForm()  # formulaire vide non lié
    return render(request, 'upload_tournoi.html', {'form': form})


@login_required
def encode_tournoi(request):
    pattern1 = re.compile("^(http[s]?:/?/?)?.*/?upload_tournoi$")
    pattern2 = re.compile("^(http[s]?:/?/?)?.*/?encode_tournoi$")
    precedent = request.META.get('HTTP_REFERER')
    # Si on arrive de la page d'encodage tournoi
    request.session['ref_ids'] = {}
    ref_ids = request.session['ref_ids']
    print('YO !')
    if pattern2.match(precedent):
        print('entrée')
        # Applique les données au travers du formulaire
        form = TournoiForm(request.POST, request.FILES)
        # Si on a correctement rempli le formulaire de tournoi
        if form.is_valid():
            print('form ok')
            # TODO Si le nom de site existe déjà tel quel ...
            request.POST.pop('csrfmiddlewaretoken')  # supprime une clé qui ne nous sert à rien

            ### SITE ORGANISATEUR : ###
            # récupère les infos non 'CommunauteJoueurs' a utiliser pour créer dans la db un objet 'site'
            prefix = 'CommunauteJoueurs_'
            orga = {k: v for k, v in request.POST.items()
                    if not k.startswith(prefix) and v}
            if 'logo' in request.FILES.keys():
                orga['logo'] = request.FILES['logo']
            orga_id = maj_ou_creation_organisation(request, post_dict=orga, session_dict='organisateur', db=Site)
            ref_ids['orga'] = orga_id

            ### CO-ORGANISATEUR : ###
            # récupère les infos préfixées 'CommunauteJoueurs_' à utiliser pour créer dans la db un objet 'site'
            # et on en profite pour nettoyer le nom des clés de ces 18 premiers caractères (CommunauteJoueurs_)
            # et on assigne rien si v est vide
            prefix = 'CommunauteJoueurs_'
            coorga = {k[len(prefix):]: v for k, v in request.POST.items()
                      if k.startswith(prefix) and v}
            if 'CommunauteJoueurs_logo' in request.FILES.keys():
                coorga['logo'] = request.FILES['CommunauteJoueurs_logo']
            coorga_id = maj_ou_creation_organisation(request, post_dict=coorga, session_dict='coorganisateur',
                                                     db=CommunauteJoueurs)
            ref_ids['coorga'] = coorga_id

            ### TOURNOI : ###
            awer = request.session['tournoi']
            dict_tournoi = {}
            tournoi_exist = Tournoi.objects.filter(timestamp=awer['timestamp'], site_id=orga_id)
            if tournoi_exist:
                print('Valeur de tournoi_exist:' + str(tournoi_exist))
                # TODO je ne sais pas encore ce qu'on ferait ^^
            else:
                cles = ['annee', 'mois', 'jour', 'timestamp', 'carte_pivotee', 'homologue']
                for cle in cles:
                    dict_tournoi[cle] = awer[cle]
                dict_tournoi['site_id'] = orga_id
                dict_tournoi['carte'] = Carte.objects.get(pk=awer['carte'])
                dict_tournoi['krosmasterset'] = KrosmasterSet.objects.get(pk=awer['krosmasterset'])
                dict_tournoi['format'] = Format.objects.get(pk=awer['format'])
                dict_tournoi['type'] = Type.objects.get(pk=awer['type'])
                dict_tournoi['encodeur'] = request.user
                if coorga_id:
                    dict_tournoi['co_organisateur_id'] = coorga_id
                newtournoi = Tournoi.objects.create(**dict_tournoi)
                newtournoi.save()
                ref_ids['tournoi'] = newtournoi.id

            ### RONDES : ###
            awer = request.session['matchs']
            rondes_ids = {}
            for num in range(1, len(awer) + 1):
                newronde = Ronde(numero=num, tournoi_id=ref_ids['tournoi'])
                newronde.save()
                rondes_ids['ronde' + str(num)] = newronde.id
            ref_ids['rondes'] = rondes_ids

            ### EQUIPEJ : ###
            awer = request.session['joueurs']
            cles = ['numero', 'd0', 'd1', 'd2', 'd3', 'classement']
            dict_equipej = {}
            equipesj_ids = {}
            for equipej in awer:
                for cle in cles:
                    dict_equipej[cle] = equipej[cle]
                newequipe = EquipeJ(**dict_equipej)
                newequipe.tournoi_id = ref_ids['tournoi']
                newequipe.save()
                equipesj_ids[str(equipej['numero'])] = newequipe.id
            equipesj_ids['999999999'] = '1'
            ref_ids['equipesj'] = equipesj_ids

            ### MATCHS : ###
            awer = request.session['matchs']
            for ronde in rondes_ids:
                for num_match in awer[ronde]:
                    eq1 = str(awer[ronde][num_match]['equipe1'])
                    eq2 = str(awer[ronde][num_match]['equipe2'])
                    res = awer[ronde][num_match]['resultat']
                    newmatch = Match(ronde_id=rondes_ids[ronde], numero=int(num_match[5:]),
                                     equipe1_id=equipesj_ids[eq1], equipe2_id=equipesj_ids[eq2], resultat=res)
                    newmatch.save()

            ### JOUEURS : ###
            awer = request.session['joueurs']
            joueurs_ids = {}
            for equipe in awer:
                joueurawer = equipe['joueur1']
                joueurdbnumcarte = Joueur.objects.filter(numero_de_carte=joueurawer['numero_de_carte'])
                joueurdbnumtmp = Joueur.objects.filter(numero_de_carte__contains='-')
                joueurdbliste = joueurdbnumtmp.filter(nom=joueurawer['nom'], prenom=joueurawer['prenom'])
                if len(joueurdbnumcarte) == 1:
                    joueurdbnumcarte = Joueur.objects.get(numero_de_carte=joueurawer['numero_de_carte'])
                    joueurs_ids[str(equipe['numero'])] = joueurdbnumcarte.id
                elif len(joueurdbliste) == 1:
                    joueurmajnumcarte = joueurdbnumtmp.get(nom=joueurawer['nom'], prenom=joueurawer['prenom'])
                    joueurmajnumcarte.numero_de_carte = joueurawer['numero_de_carte']
                    joueurmajnumcarte.save()
                    joueurs_ids[str(equipe['numero'])] = joueurmajnumcarte.id
                else:
                    joueur_dict = {}
                    cles = ['nom', 'numero_de_carte', 'prenom']
                    for cle in cles:
                        joueur_dict[cle] = joueurawer[cle]
                    newjoueur = Joueur(**joueur_dict)
                    newjoueur.save()
                    joueurs_ids[str(equipe['numero'])] = newjoueur.id
            ref_ids['joueurs'] = joueurs_ids

            ### LIAISON EQUIPEJ-JOUEUR ###
            equipes = ref_ids['equipesj']
            joueurs = ref_ids['joueurs']
            liste_eqj = []
            eqj = {}
            # on utilise la liste de clés des joueurs car ne contient pas de référence à l'equipej Bye (999999999)
            # contrairement à la liste de clés des equipesj qui en a besoin
            for cle in joueurs:
                joueurdb = Joueur.objects.get(pk=joueurs[cle])
                equipedb = EquipeJ.objects.get(pk=equipes[cle])
                equipedb.joueurs.add(joueurdb)
                eqj[joueurdb.id] = equipedb.id
            liste_eqj.append(eqj)
            request.session['eqj'] = liste_eqj

            ### CREATION DU DICT DE SESSION PSEUDO ###
            dict_session_joueurs = request.session['joueurs']
            liste_dict_pseudos = []
            for equipe in dict_session_joueurs:
                joueur_infos_dict = {}
                # reprend du dictionnaire de session actuel
                joueur_infos_dict['classement'] = equipe['classement']
                joueur_infos_dict['d0'] = equipe['d0']
                numcarte = equipe['joueur1']['numero_de_carte']
                # va chercher dans la db
                joueurdb = Joueur.objects.get(numero_de_carte=numcarte)
                joueur_infos_dict['displayname'] = joueurdb.displayname
                joueur_infos_dict['nom'] = joueurdb.nom
                joueur_infos_dict['prenom'] = joueurdb.prenom
                joueur_infos_dict['pseudo'] = joueurdb.pseudo
                joueur_infos_dict['pk'] = joueurdb.id
                liste_dict_pseudos.append(joueur_infos_dict)
            request.session['pseudos'] = liste_dict_pseudos

            ### AJOUT ID TOURNOI & NETTOYAGE DES DICT DE SESSION INUTILES ###
            tournoi = ref_ids['tournoi']
            request.session['tournoi'] = tournoi
            cles_de_session = ['coorganisateur', 'joueurs', 'matchs', 'organisateur', 'ref_ids']
            for cle in cles_de_session:
                if cle in request.session:
                    del request.session[cle]

            return HttpResponseRedirect('encode_pseudos')
        # Si des contraintes ne sont pas rencontrées
        else:
            print('form NOK')
            return render(request, 'encode_tournoi.html',
                          {'form': form})
    # Si on arrive d'upload tournoi
    elif pattern1.match(precedent):
        print('1er arrivage')
        form = TournoiForm(request.session['tournoi'])
        return render(request, 'encode_tournoi.html',
                      {'form': form})
    # Si on ne vient pas d'un endroit autorisé on repart à l'upload de tournoi
    else:
        print('take the good way bastard')
        return HttpResponseRedirect('upload_tournoi')


@login_required
def encode_pseudos(request):
    # on vérifie que la page précédente était http(s)://.../encode_tournoi
    # pattern = re.compile("^(http[s]?:\/?\/?)?.*\/?encode_tournoi$")
    pattern1 = re.compile("^(http[s]?:/?/?)?.*/?encode_tournoi$")
    pattern2 = re.compile("^(http[s]?:/?/?)?.*/?encode_pseudos$")
    precedent = request.META.get('HTTP_REFERER')
    if pattern1.match(precedent) or pattern2.match(precedent):
        if 'action' in request.POST and request.POST['action'] == "Etape 3: Equipes":
            for cle in request.POST:
                # IMPORTANT, la QueryDict du POST ne retourne que la dernière valeur associée à une clé
                # pour avoir un tableau, il faut requérir spécifiquement une liste avec .getlist
                if cle != 'action' and cle != 'csrfmiddlewaretoken':
                    prenom = request.POST.getlist(cle)[0]
                    nom = request.POST.getlist(cle)[1]
                    pseudo = request.POST.getlist(cle)[2]
                    displayname = prenom + " " + nom[0] + "."
                    joueurdb = Joueur.objects.get(pk=cle)
                    if not joueurdb.displayname:
                        joueurdb.prenom = prenom
                        joueurdb.nom = nom
                        joueurdb.displayname = displayname
                    if pseudo:
                        joueurdb.pseudo = pseudo
                    joueurdb.save()
            return HttpResponseRedirect('encode_equipes')
        else:
            joueur_tmp = Joueur.objects.get(pk='3')
            joueur_tmp2 = joueur_tmp.equipe_joueur.all()
            for equipe in joueur_tmp2:
                print(equipe.id)
                print(equipe.classement)

            return render(request, 'encode_pseudos.html')

    return HttpResponseRedirect('auth_login')


@login_required
def encode_equipes(request):
    pattern1 = re.compile("^(http[s]?:/?/?)?.*/?encode_pseudos$")
    pattern2 = re.compile("^(http[s]?:/?/?)?(.*/?encode_equipes)?($|\?{1}eq=[0-9]+)$")
    precedent = request.META.get('HTTP_REFERER')
    if pattern1.match(precedent) or pattern2.match(precedent):
        if request.POST:
            eqk = {}
            for cle in request.POST:
                print(cle)
                if re.match(r'^eqk\[?[1-8]\]\[numero\]', cle) is not None:
                    eqk[cle[4:5]] = request.POST[cle]
            eqk['eqj'] = request.POST['eqj']
            eq_nbrfigs = len(eqk) - 1
            # Création de l'identifiant d'équipe
            eq_ident = ""
            eq_init = 0
            eq_boss = False
            krosfigs = []
            for i in range(1, eq_nbrfigs + 1):
                k_num = eqk[str(i)]
                kdb = Krosmaster.objects.get(numero=k_num)
                eq_ident = eq_ident + k_num
                eq_init += kdb.initiative
                eq_boss = eq_boss or kdb.boss
                k_id = kdb.id
                k_nb = 0
                for j in eqk.values():
                    if j == k_num:
                        k_nb += 1
                krosfigs.append({'id': k_id, 'nbr': k_nb})
            if eq_nbrfigs < 8:
                for i in range(eq_nbrfigs + 1, 9):
                    eq_ident = eq_ident + '0000'
            eqk_test = EquipeK.objects.filter(identifiant__iexact=eq_ident)
            if not eqk_test:
                eq_now = EquipeK.objects.create(identifiant=eq_ident, total_init=eq_init,
                                                total_krosmaster=eq_nbrfigs, boss=eq_boss)
                eq_now.save()
                ### Liaison de l'Equipe K à ses Krosmasters
                #
                # supprime d'abord les doublons sinon on se retrouve avec xfois la même ligne
                # pour x figurines présentent xfois dans l'équipe
                list({v['id']: v for v in krosfigs}.values())
                for k in krosfigs:
                    km = Krosmaster.objects.get(pk=k['id'])
                    ek_k = EquipeK_Krosmaster.objects.create(equipek=eq_now, krosmaster=km, nombrek=k['nbr'])
                    ek_k.save()
            else:
                eq_now = EquipeK.objects.get(identifiant=eq_ident)

            # Liaison de l'Equipe J à l'Equipe K
            ej = EquipeJ.objects.get(pk=eqk['eqj'])
            if ej.equipe_krosmasters is None:
                ej.equipe_krosmasters = eq_now
                ej.save()
            else:
                print(ej.equipe_krosmasters)
                ej.equipe_krosmasters = eq_now
                ej.save(force_update=True) # nécessaire pour le retour arrière lors de l'encodage des équipes
            return HttpResponse("ok")
        else:
            # On construit les pages et on les gère
            eqj = request.session['eqj'][0]
            print(eqj)
            liste_eqj = []
            for j in eqj:
                print(j)
                dict_eq = {}
                joueurdb = Joueur.objects.get(pk=j)
                if joueurdb.pseudo:
                    dict_eq['joueur'] = joueurdb.pseudo + ' (' + joueurdb.displayname + ')'
                else:
                    dict_eq['joueur'] = joueurdb.displayname
                dict_eq['eqj'] = eqj[j]
                dict_eq['classement'] = EquipeJ.objects.get(pk=eqj[j]).classement
                dict_eq['resistance'] = EquipeJ.objects.get(pk=eqj[j]).d1
                liste_eqj.append(dict_eq)
            print(liste_eqj)
            # ordre_eqj = sorted(liste_eqj, key=itemgetter('classement'))
            ordre_eqj = sorted(liste_eqj, key = lambda x: (x['classement'], -x['resistance']))
            print(ordre_eqj)
            paginator = Paginator(ordre_eqj, 1)
            maxpage = paginator.num_pages

            page = request.GET.get('eq')
            ks_id = Tournoi.objects.get(pk=request.session['tournoi']).krosmasterset.id
            try:
                equipe = paginator.page(page)
            except PageNotAnInteger:
                # Si page n'est pas un entier, on envoit la première page.
                equipe = paginator.page(1)
            except EmptyPage:
                # Si le n° de page n'existe pas, on file la dernière
                equipe = paginator.page(maxpage)

            return render(request, 'encode_equipes.html',
                          {'equipe': equipe,
                           'maxpage': maxpage,
                           'ks_id': ks_id,
                           })
    else:
        return HttpResponseRedirect('upload_tournoi')

    return HttpResponseRedirect('auth_login')

@login_required
def encode_fin(request):
    # pattern = re.compile("^(http[s]?:/?/?)?(.*/?encode_equipes)?($|\?{1}eq=[0-9]+)$")
    # precedent = request.META.get('HTTP_REFERER')
    # if pattern.match(precedent):
    t_id = request.GET['t']
    logout(request)

    couleur = {'1': "orange", '2': "silver", '3': "brown"}
    rep_img_ogp = "/ogp/"
    base = Image.open(settings.STATIC_ROOT + "/kma_reports/img/ogp_gabarit.png")
    logo_initiative_tmp = Image.open(settings.STATIC_ROOT + "/kma_reports/img/initiative.png")
    logo_initiative = logo_initiative_tmp.resize((16, 16), Image.ANTIALIAS)

    tournoi = Tournoi.objects.get(id=t_id)
    site_tmp = Image.open(tournoi.site.logo)
    site = site_tmp.resize((125, 125), Image.ANTIALIAS)
    carte_tmp = Image.open(tournoi.carte.image)
    carte_tmp2 = carte_tmp.resize((125, 125), Image.ANTIALIAS)
    if tournoi.carte_pivotee:
        carte = carte_tmp2.rotate(90)
    else:
        carte = carte_tmp2
    drapeau = Image.open(tournoi.site.pays.drapeau)
    drapeau_long, drapeau_haut = drapeau.size
    krosmasterset_en = Image.open(settings.STATIC_ROOT + "/kma_reports/img/krosmasterset/en/" + str(tournoi.krosmasterset.id) + ".png")
    ks_en_long, ks_en_haut = krosmasterset_en.size
    krosmasterset_fr = Image.open(settings.STATIC_ROOT + "/kma_reports/img/krosmasterset/fr/" + str(tournoi.krosmasterset.id) + ".png")
    ks_fr_long, ks_fr_haut = krosmasterset_fr.size
    eqjs = tournoi.equipej_set.order_by("classement")
    nbrj = len(eqjs)
    eqjs3 = eqjs[:3]
    top3 = []
    for eq in eqjs3:
        eqk = liste_k(eq.equipe_krosmasters.identifiant, eq.equipe_krosmasters.total_krosmaster)
        nbrk = len(eqk)
        listek = []
        for k in eqk:
            print(k[0][:3])
            listek.append(k[0][:3] + "a.icone.png")

        joueur = {'classement': eq.classement,
                  'd0': eq.d0,
                  'd1': "{0:.2f}".format(eq.d1),
                  'nom': eq.joueur_affiche(),
                  'init': eq.equipe_krosmasters.total_init,
                  'nbrk': nbrk,
                  'figs': listek
                  }
        top3.append(joueur)
    try:
        base.paste(site, (26, 28), site)
    except ValueError:
        base.paste(site, (26, 28))
    base.paste(carte, (26, 160))
    base.paste(drapeau, (167, 27))
    y = 0
    for j in top3:
        ImageDraw.Draw(base).text((183, 106 + y * 63), str(j['classement']), couleur[str(y + 1)],
                                  font=ImageFont.truetype(settings.STATIC_ROOT + "/kma_reports/Eurosti.ttf", 44))
        ImageDraw.Draw(base).text((228, 96 + y * 63), str(j['nom']), "Black",
                                  font=ImageFont.truetype(settings.STATIC_ROOT + "/kma_reports/Eurosti.ttf", 20))
        ImageDraw.Draw(base).text((570, 106 + y * 63), str(j['init']), "Purple",
                                  font=ImageFont.truetype(settings.STATIC_ROOT + "/kma_reports/Eurosti.ttf", 16))
        ImageDraw.Draw(base).text((552, 126 + y * 63), str(j['d0']) + "Pts", "Black",
                                  font=ImageFont.truetype(settings.STATIC_ROOT + "/kma_reports/Eurosti.ttf", 14))
        ImageDraw.Draw(base).text((554, 142 + y * 63), str(j['d1']) + "%", "Black",
                                  font=ImageFont.truetype(settings.STATIC_ROOT + "/kma_reports/Eurosti.ttf", 12))
        base.paste(logo_initiative, (552, 106 + y * 63), logo_initiative)
        x = 0
        for k in j['figs']:
            fig_tmp = Image.open(settings.STATIC_ROOT + '/kma_reports/img/krosmasters/' + k)
            fig = fig_tmp.resize((40, 40), Image.ANTIALIAS)
            base.paste(fig, (228 + x * 40, 116 + y * 63), fig)
            x += 1
        y += 1

    base_fr = base
    base_en = copy.copy(base)
    date = datetime(tournoi.annee, tournoi.mois, tournoi.jour).date()

    locale.setlocale(locale.LC_TIME, 'en_US.utf8')
    ImageDraw.Draw(base_en).text((167 + drapeau_long + 7, 27), date.strftime("%B %d, %Y"), "black", font=ImageFont.truetype(settings.STATIC_ROOT + "/kma_reports/Eurosti.ttf", 24))
    base_en.paste(krosmasterset_en, (167, 60), krosmasterset_en)
    t_type_j = str(nbrj) + ' Players'
    ImageDraw.Draw(base_en).text((167 + ks_en_long + 7, 60), str(t_type_j), "black", font=ImageFont.truetype(settings.STATIC_ROOT + "/kma_reports/Eurosti.ttf", 24))
    base_en.save(settings.MEDIA_ROOT + rep_img_ogp + 't.' + str(t_id) + '.en.png')

    locale.setlocale(locale.LC_TIME, 'fr_BE.utf8')
    ImageDraw.Draw(base_fr).text((167 + drapeau_long + 7, 27), date.strftime("%d %B %Y"), "black", font=ImageFont.truetype(settings.STATIC_ROOT + "/kma_reports/Eurosti.ttf", 24))
    base_fr.paste(krosmasterset_fr, (167,60), krosmasterset_fr)
    t_type_j = str(nbrj) + ' Joueurs'
    ImageDraw.Draw(base_fr).text((167 + ks_fr_long + 7, 60), str(t_type_j), "black", font=ImageFont.truetype(settings.STATIC_ROOT + "/kma_reports/Eurosti.ttf", 24))
    base_fr.save(settings.MEDIA_ROOT + rep_img_ogp + 't.' + str(t_id) + '.fr.png')

    logout(request)
    return render(request, 'encode_fin.html', {'t': t_id})
    # else:
    #     return HttpResponseRedirect('upload_tournoi')

# def annee(request):
#     tournois = []
#     annee = request.get_full_path()[-4:]
#     liste_t = Tournoi.objects.filter(annee=annee)
#     if len(liste_t) > 1:
#         for t in liste_t:
#             tmp = {}
#             tmp['mois'] = t.mois
#             tmp['jour'] = t.jour
#             tmp['site'] = t.site
#             tmp['site_id'] = t.site_id
#             tmp['id'] = t.id
#             tournois.append(tmp)
#         return render(request, 'annee.html',
#                       {'annee': annee,
#                        'tournois': tournois,
#                        })
#     elif len(liste_t) == 1:
#         return HttpResponseRedirect('affichage_tournoi?t=' + str(liste_t[0].id))
#     else:
#         return HttpResponseRedirect('accueil')
#
#
# def mois(request):
#     tournois = []
#     annee = request.get_full_path()[-7:-3]
#     mois = request.get_full_path()[-2:]
#     liste_t = Tournoi.objects.filter(annee=annee, mois=mois)
#     if len(liste_t) > 1:
#         for t in liste_t:
#             tmp = {}
#             tmp['jour'] = t.jour
#             tmp['site'] = t.site
#             tmp['site_id'] = t.site_id
#             tmp['id'] = t.id
#             tournois.append(tmp)
#         return render(request, 'mois.html',
#                       {'annee': annee,
#                        'mois': mois,
#                        'tournois': tournois,
#                        })
#     elif len(liste_t) == 1:
#         return HttpResponseRedirect('../affichage_tournoi?t=' + str(liste_t[0].id))
#     else:
#         return HttpResponseRedirect('../accueil')
#
#
# def jour(request):
#     tournois = []
#     annee = request.get_full_path()[-10:-6]
#     mois = request.get_full_path()[-5:-3]
#     jour = request.get_full_path()[-2:]
#     liste_t = Tournoi.objects.filter(annee=annee, mois=mois, jour=jour)
#     if len(liste_t) > 1:
#         for t in liste_t:
#             tmp = {}
#             tmp['site'] = t.site
#             tmp['site_id'] = t.site_id
#             tmp['id'] = t.id
#             tournois.append(tmp)
#         return render(request, 'mois.html',
#                       {'annee': annee,
#                        'mois': mois,
#                        'jour': jour,
#                        'tournois': tournois,
#                        })
#     elif len(liste_t) == 1:
#         return HttpResponseRedirect('../../affichage_tournoi?t=' + str(liste_t[0].id))
#     else:
#         return HttpResponseRedirect('../../accueil')


def affichage_carte(request):
    try:
        carte = Carte.objects.get(pk=request.GET['c'])
    except ObjectDoesNotExist:
        return HttpResponseRedirect('accueil')

    # TODO: tout est à faire, ici simple copier-coller d'équipe
    # equipe = {'k': liste_k(nomeqk=eqk.identifiant, nbrfig=eqk.total_krosmaster),
    #           'i': eqk.total_init,
    #           'b': eqk.boss,
    #           'ideqk': eqk.id
    #           }
    #
    # liste_eqj = eqk.equipej_set.all()
    # liste_j = []
    # tournois = []
    # for eqj in liste_eqj:
    #     j = {'id': eqj.joueurs.all()[0].id}
    #     j['nom'] = eqj.joueur_affiche()
    #     liste_j.append(j)
    #     t = eqj.tournoi
    #     tournoi = {'t': t,
    #                'd': datetime(t.annee, t.mois, t.jour).date(),
    #                'm': t.carte,
    #                'ty': t.type.nom,
    #                's': t.site.nom,
    #                'c': eqj.classement,
    #                'p': len(t.equipej_set.all())
    #                }
    #     tournois.append(tournoi)
    #
    # compteur = {}
    # for j in liste_j:
    #     if j['id'] not in compteur:
    #         compteur[j['id']] = 0
    #     compteur[j['id']] = compteur[j['id']] + 1
    # liste_j_u = list(unique_everseen(liste_j, key=itemgetter('id')))
    # for j in liste_j_u:
    #     j['nbrx'] = compteur[j['id']]
    #
    # top1 = 0
    # top4 = 0
    # for tournoi in tournois:
    #     if tournoi['c'] < 5:
    #         top4 += 1
    #         if tournoi['c'] < 2:
    #             print(tournoi['c'])
    #             top1 += 1
    # stats = {'alignee': len(liste_eqj),
    #          'top1': top1,
    #          'top4': top4
    #          }

    return render(request, 'affichage_carte.html',
                  {'carte': carte
                   })



def affichage_equipe(request):
    try:
        eqk = EquipeK.objects.get(pk=request.GET['e'])
    except ObjectDoesNotExist:
        return HttpResponseRedirect('accueil')

    equipe = {'k': liste_k(nomeqk=eqk.identifiant, nbrfig=eqk.total_krosmaster),
              'i': eqk.total_init,
              'b': eqk.boss,
              'ideqk': eqk.id
              }

    liste_eqj = eqk.equipej_set.all()
    liste_j = []
    tournois = []
    for eqj in liste_eqj:
        j = {'id': eqj.joueurs.all()[0].id}
        j['nom'] = eqj.joueur_affiche()
        liste_j.append(j)
        t = eqj.tournoi
        tournoi = {'t': t,
                   'd': datetime(t.annee, t.mois, t.jour).date(),
                   'm': t.carte,
                   'ty': t.type.nom,
                   's': t.site.nom,
                   'c': eqj.classement,
                   'p': len(t.equipej_set.all())
                   }
        tournois.append(tournoi)

    compteur = {}
    for j in liste_j:
        if j['id'] not in compteur:
            compteur[j['id']] = 0
        compteur[j['id']] = compteur[j['id']] + 1
    liste_j_u = list(unique_everseen(liste_j, key=itemgetter('id')))
    for j in liste_j_u:
        j['nbrx'] = compteur[j['id']]

    top1 = 0
    top4 = 0
    for tournoi in tournois:
        if tournoi['c'] < 5:
            top4 += 1
            if tournoi['c'] < 2:
                print(tournoi['c'])
                top1 += 1
    stats = {'alignee': len(liste_eqj),
             'top1': top1,
             'top4': top4
             }

    return render(request, 'affichage_equipe.html',
                  {'joueurs': liste_j_u,
                   'stats': stats,
                   'tournois': tournois,
                   'eq': equipe
                   })


def affichage_krosmaster(request):
    # On essaye de trouver le Krosmaster demandé
    try:
        k = Krosmaster.objects.get(numero=request.GET['k'])
        kset = KrosmasterSet.objects.get(krosmaster=k)
    except (ObjectDoesNotExist, MultiValueDictKeyError):
        return HttpResponseRedirect('accueil')
    # On cherche les autres krosmaster correspondants à un moment de leur histoire (boss, plus boss, eternel, saison, ...)
    aka = Krosmaster.objects.filter(numero__startswith=k.numero[:3]).exclude(Q(id=k.id) | Q(krosmasterset=5))
    # actuel = ""
    # ban = False
    # if not k.autorise:
    #     autres_v = Krosmaster.objects.filter(numero__contains=k.numero[:3]).filter(autorise=1)
    #     if autres_v:
    #         actuel = autres_v[0] #filter retourne une liste, on prend le seul item attendu de celle-ci
    #     else:
    #         ban = True
    # TODO Faire évoluer krosmaster pour lier directement à la table KS cô Tournois
    # Utilise les id de KrosmasterSet sur base du nombre de d'étoiles pour préparer la requête suivante
    # ks = k.etoiles if k.etoiles > 0 else 4
    eqk_differentes = EquipeK.objects.filter(equipej__tournoi__krosmasterset__nom=kset.nom)
    # Cherche le nombre d'équipes jouées (1 équipe pouvant être jouée plusieur fois dans un même tournoi ou des différents)
    eqk_jouees = EquipeJ.objects.filter(tournoi__krosmasterset__nom=kset.nom)
    if kset.nom == "eternel":
        eqk_differentes = eqk_differentes.exclude(equipej__tournoi__krosmasterset__max_etoiles__lte=kset.max_etoiles)
        eqk_jouees = eqk_jouees.exclude(tournoi__krosmasterset__max_etoiles__lte=kset.max_etoiles)
    nbr_eqk_differentes = eqk_differentes.count()
    nbr_eqk_jouees = eqk_jouees.count()

    nbr_eqk_incluantes = 0
    equipe = {3: [], 2: [], 1: []}
    for compteur in range(3, 0, -1):
        if k.nombre_max >= compteur:
            # eqk_kx = EquipeK_Krosmaster.objects.filter(Q(krosmaster_id=k.id) & Q(nombrek=compteur)).distinct()
            liste_eqk = EquipeK.objects.filter(Q(equipek_krosmaster__krosmaster=k) & Q(equipek_krosmaster__nombrek=compteur) & Q(equipej__tournoi__krosmasterset__nom=kset.nom) & Q(equipej__tournoi__krosmasterset__max_etoiles__gte=kset.max_etoiles)).distinct()
            # for eqk_k in eqk_kx:
            #     liste_eqk = EquipeK.objects.filter(Q(equipek_krosmaster=eqk_k) & Q(equipej__tournoi__krosmasterset__nom=kset.nom) & Q(equipej__tournoi__krosmasterset__max_etoiles__gte=kset.max_etoiles)).distinct()
            for eqk in liste_eqk:
                # Crée un dictionnaire d'équipe formaté contenant les valeurs DB
                eq = objet_eqk(eqk)
                # On y ajoute des infos calculées
                compte = EquipeJ.objects.filter(equipe_krosmasters_id=eqk.id).count()
                eq['compte'] = compte
                nbr_eqk_incluantes += compte
                # On place le dictionnaire représentant l'équipe dans le dictionnaire les classant par occurence de krosmaster (3, 2, 1)
                equipe[compteur].append(eq)
    # print(nbr_eqk_incluantes)
    presence = nbr_eqk_incluantes / nbr_eqk_jouees * 100 if nbr_eqk_jouees != 0 else 0
    utilite = nbr_eqk_incluantes / nbr_eqk_differentes * 100 if nbr_eqk_differentes != 0 else 0

    return render(request, 'affichage_krosmaster.html',
                  {'k': k,
                   'aka': aka,
                   'eq3': equipe[3],
                   'eq2': equipe[2],
                   'eq1': equipe[1],
                   'presence': presence,
                   'utilite': utilite,
                   # 'actuel': actuel,
                   # 'ban': ban
                   })


def liste_tournois(request):
    if request.POST:
        joueur = ''
        p = {k: v for k, v in request.POST.items() if v != ""}
        p.pop('csrfmiddlewaretoken')
        # annee, mois, site, coorg - ne nécessite aucune modification du dictionnaire obtenu par POST
        # type, carte, pays - nécessite d'obtenir les identifiant sur base des noms
        if 'mois' in p and p['mois'] == '0':    # si la valeur de mois est 0, il faut tous les afficher
            p.pop('mois')                       # on supprime donc le filtre du dictionnaire
        if 'pays' in p:                         # on sort le pays du filtre pour le passer directement en variable nommée
            pays = Pays.objects.get(nom=p['pays']).id
            p.pop('pays')
        else:
            pays = ''
        tournois = liste_t2(pays=pays, joueur=joueur, **p)
        p['pays'] = pays
        form = Recherche_T(p)
    else:
        return HttpResponseRedirect('accueil')

    return render(request, 'liste_tournois.html',
                  {'tournois': tournois, 'form': form})

def top_equipes(request):
    liste_equipes_k = EquipeK.objects.all()

    eqkx = []
    for eqk in liste_equipes_k:
        eqjs = eqk.equipej_set.all()
        nbrx_inscrite = len(eqjs)
        eqkx.append({'eqk': eqk,
                     'nbrx': nbrx_inscrite})
    eqkx = sorted(eqkx, key=itemgetter('nbrx'), reverse=True)[0:10]
    for item in eqkx:
        item['k'] = liste_k(nomeqk=item['eqk'].identifiant, nbrfig=item['eqk'].total_krosmaster)
        item['ideqk'] = item['eqk'].id

    eqkp = []
    for eqk in liste_equipes_k:
        eqjs = eqk.equipej_set.filter(classement=1)
        nbrx_remporte = len(eqjs)
        if nbrx_remporte > 0:
            maps = []
            for eqj in eqjs:
                maps.append(eqj.tournoi.carte)
            eqkp.append({'eqk': eqk,
                         'nbrx': nbrx_remporte,
                         'maps': maps})
    for item in eqkp:
        item['k'] = liste_k(nomeqk=item['eqk'].identifiant, nbrfig=item['eqk'].total_krosmaster)
        item['ideqk'] = item['eqk'].id

    return render(request, 'top_equipes.html',
                  {'topnbrx': eqkx,
                   'topperf': eqkp
                   })


def affichage_joueur(request):
    if 'j' in request.GET and request.GET['j'] != '1':
        jid = request.GET['j']
        try:
            joueur = Joueur.objects.get(pk=jid)
        except ObjectDoesNotExist:
            return HttpResponseRedirect('accueil')
    else:
        return HttpResponseRedirect('accueil')

    if joueur.pseudo:
        display = joueur.pseudo + ' '
        add = '[ ' + joueur.displayname + ' ]'
    else:
        display = joueur.displayname
        add = ''
    eq_toutes_infos = []
    liste_eq_obj = []
    for eqj_id in joueur.equipe_joueur.all():
        t = eqj_id.tournoi
        date = datetime(t.annee, t.mois, t.jour).date()
        k = eqj_id.equipe_krosmasters
        m = eqj_id.tournoi.carte
        eqk_inf = {'timestamp': t.timestamp,
                   'date': date,
                   't': t,
                   'k': k,
                   'm': m}
        eq_toutes_infos.append(eqk_inf)
        liste_eq_obj.append(k)
    compteur = {}
    for eq_obj in liste_eq_obj:
        k = eq_obj
        if k not in compteur:
            compteur[k] = 0
        compteur[k] = compteur[k] + 1
    eq_toutes_infos.sort(key=itemgetter('timestamp'), reverse=True)
    eq_u = list(unique_everseen(eq_toutes_infos, key=itemgetter('k')))
    print(eq_u)
    equipes = [dict(s) for s in eq_u]
    for eq_dict in equipes:
        obj = eq_dict['k']
        eq_dict['compte'] = compteur[obj]
    for eq in equipes:
        print(eq['m'])
        # nbrfig = eq['k'].total_krosmaster
        # figs = [eq['k'].identifiant[i:i + 4] for i in range(0, nbrfig * 4, 4)]
        if 'k' in eq and eq['k'] is not None:
            eq['ideqk'] = eq['k'].id
            eq['k'] = liste_k(nomeqk=eq['k'].identifiant, nbrfig=eq['k'].total_krosmaster)
    tournois = liste_t(clef='joueur', valeur=jid)
    return render(request, 'affichage_joueur.html',
                  {'equipes': equipes,
                   'display': display,
                   'add': add,
                   'tournois': tournois
                   })


def affichage_orga(request):
    if 'o' in request.GET:
        clef = 'site'
        try:
            infos = construct_orga(type='o', numid=request.GET['o'])
        except (HttpResponseRedirect, ValueError, TypeError):
            return HttpResponseRedirect('accueil')
    elif 'c' in request.GET:
        clef = 'co_organisateur'
        try:
            infos = construct_orga(type='c', numid=request.GET['c'])
        except (HttpResponseRedirect, ValueError, TypeError):
            return HttpResponseRedirect('accueil')
    else:
        return HttpResponseRedirect('accueil')


    obj = {str(clef): infos['principal'].id}
    tournois = derniers_t(type='orga', nombre=5, **obj)

    return render(request, 'affichage_orga.html',
                  {'tournois': tournois,
                   'principal': infos['principal'],
                   'drapeau': infos['drapeau'],
                   'sub': infos['sub']
                   })


def affichage_tournoi(request):
    ### Nettoyage des Dict de session inutils quand on vient de l'encodage###
    cles_de_session = ['tournoi', 'pseudos', 'eqj']
    for cle in cles_de_session:
        if cle in request.session:
            del request.session[cle]
    ### Récupère l'ID du tournoi souhaité ###
    try:
        t_id = request.GET.get('t')
        tournoi = Tournoi.objects.get(pk=t_id)
    except ObjectDoesNotExist:
        return HttpResponseRedirect('accueil')

    listes_equipes_j = tournoi.equipej_set.all()
    rondes = Ronde.objects.filter(tournoi=tournoi)
    nbr_rondes = len(rondes)
    max_pts = nbr_rondes * 3
    classement = []
    for eqj in listes_equipes_j:
        classement.append(objet_eqj(eqj, max_pts))
    # compatibilité avec les autres formes d'affichage :-(
    t = {'classement': classement}
    rencontres = []
    for ronde in rondes:
        r = {}
        r['num'] = ronde.numero
        r['m'] = []
        matchs = Match.objects.filter(ronde=ronde.id)
        for match in matchs:
            m = {}
            m['num'] = match.numero
            m['eq1'] = match.equipe1.joueur_affiche()
            m['id1'] = match.equipe1.joueurs.all()[0].id
            m['eq2'] = match.equipe2.joueur_affiche()
            m['id2'] = match.equipe2.joueurs.all()[0].id
            m['res'] = match.resultat
            r['m'].append(m)
        rencontres.append(r)
    infost = {}
    infost['pays'] = tournoi.site.pays
    infost['date'] = datetime(tournoi.annee, tournoi.mois, tournoi.jour).date()
    infost['site'] = tournoi.site
    infost['type'] = tournoi.type
    if tournoi.krosmasterset:
        infost['krosmasterset'] = tournoi.krosmasterset
    if tournoi.co_organisateur:
        infost['coorg'] = tournoi.co_organisateur
    infost['carte'] = tournoi.carte
    infost['rotation'] = tournoi.carte_pivotee

    return render(request, 'affichage_tournoi.html',
                  {'tournoi': infost,
                   't': t,
                   'rencontres': rencontres,
                   't_id': t_id
                   })


def tournoi_awrx(xml, carte, krosmasterset):
    clefs_dict = {"generated": 'date',
                  "homologated": 'homologue',
                  "name": 'nom_tmp',
                  "address": 'adresse',
                  "zipcode": 'code_postal',
                  "country": 'pays',
                  "format": 'format',
                  "type": 'type',
                  "teamStructure": 'structuredequipe'
                  }
    tournoi_dict = {}
    cartedb = Carte.objects.get(pk=carte['id'])
    krosmastersetdb = KrosmasterSet.objects.get(pk=krosmasterset['id'])
    tournoi_dict.update({'carte': cartedb.id,
                         'carte_pivotee': carte['rotation'],
                         'carte_nom': cartedb.nom,
                         'carte_img': str(cartedb.image),
                         'krosmasterset': krosmastersetdb.id,
                         'krosmasterset_nom': krosmastersetdb.nom,
                         'krosmasterset_etoiles': krosmastersetdb.max_etoiles,
                         })
    infos = xml.xpath("/tournament")  # liste d'1 élémént, on l'appelera infos[0] au lieu d'utiliser 1 boucle "for"
    # Pour chaque cléf utile que l'on souhaite récupérer du fichier AWER
    for clef in clefs_dict.keys():
        # on récupère la valeur francophile qui servira de clef dans notre propre dictionnaire
        objet = clefs_dict[clef]
        # si il s'agit de la date, il y a un traitement à faire
        if objet == 'date':
            # la date obtenue est une string (Sun Feb 24 19:24:52 CET 2016),
            # on la converti en date dans un contexte UTC(EN)
            dt_obj = dateutil.parser.parse(infos[0].get(clef))
            locale.setlocale(locale.LC_ALL, "fr_BE.UTF-8")
            tournoi_dict.update({'date': str(dt_obj),
                                 # les objets datetime ne sont pas serializable en JSON pour récupérer un objet datetime
                                 # il faudra utiliser : datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S")
                                 'annee': dt_obj.year,
                                 'jour': dt_obj.strftime('%d'),  # le strftime est indispensable pour avoir 01 et pas 1
                                 'mois': dt_obj.strftime('%m'),  # le strftime est indispensable pour avoir 01 et pas 1
                                 'jourtxt': dt_obj.strftime('%A'),
                                 'moistxt': dt_obj.strftime('%B'),
                                 'timestamp': int(time.mktime(dt_obj.timetuple()))
                                 })
            if len(Tournoi.objects.filter(timestamp=tournoi_dict['timestamp'])) > 0:
                print(len(Tournoi.objects.filter(timestamp=tournoi_dict['timestamp'])))
                tournoi_dict['existe'] = True
        # dans les autres cas on récupère la valeur et on l'ajoute à notre dictionnaire avec notre clé francophile
        # elif objet == 'codepostal':
        #     tournoi_dict.update({'site_code_postal': infos[0].get(clef)})
        elif objet == 'homologue':
            if infos[0].get(clef) == 'true':
                homologue_booleen = True
            else:
                homologue_booleen = False
            tournoi_dict.update({objet: homologue_booleen})
        else:
            tournoi_dict.update({objet: infos[0].get(clef)})
    return tournoi_dict


def joueurs_awrx(xml):
    infos = xml.xpath("/tournament/players/team")
    equipe_list = []
    for equipe in infos:
        jid = 0
        eid = equipe.get('id')
        if str(eid) in earj:    # On ne procède que si l'équipe a vraiment joué !
            einfos = {"numero": int(eid),
                      "d0": int(float(equipe.get('d0'))),
                      "d1": float(equipe.get('d1')),
                      "d2": float(equipe.get('d2')),
                      "d3": float(equipe.get('d3'))}
            for joueur in equipe:
                jid += 1
                initiales = ""
                pseudo = ""
                awer_num = joueur.get('numero')
                if Joueur.objects.filter(numero_de_carte=awer_num):
                    j_obj = Joueur.objects.get(numero_de_carte=awer_num)
                    if j_obj.pseudo:
                        # prenom = j_obj.first_name
                        # nom = j_obj.last_name
                        pseudo = j_obj.pseudo
                        # subst = True
                for i in joueur.get('lastname').upper().split('-'):
                    initiales += i[0]
                prenom = joueur.get('firstname')
                nom = joueur.get('lastname').upper()
                initiales += "."
                displayname = joueur.get('firstname').title() + " " + initiales
                # subst = False
                jinfos = {"prenom": prenom,
                          "nom": nom,
                          "displayname": displayname,
                          "pseudo": pseudo,
                          "numero_de_carte": awer_num}
                einfos.update({"joueur{}".format(jid): jinfos})  # formatage en passant l'INT ou "joueur" + str(jid4str)
            # equipe_dict.update({"{}".format(eid): einfos})  # on ne format pas 'équipe' pour accéder directement à l'id
            equipe_list.append(einfos)
        else:
            print(str(eid) + " n'est pas dans la liste")
    equipe_ordo = sorted(equipe_list, key=lambda t: (float(t['d0']), float(t['d1']), float(t['d2']), float(t['d3'])),
                         reverse=True)
    # equipe_odict = OrderedDict(sorted(equipe_dict.items(),
    #                                   key=lambda t: (float(t[1]['d0']),
    #                                                  float(t[1]['d1']),
    #                                                  float(t[1]['d2']),
    #                                                  float(t[1]['d3'])),
    #                                   reverse=True))
    #  c = 1
    # for item in equipe_odict:
    #     i = equipe_odict[item]
    #     for seconditem in equipe_odict:
    #         s = equipe_odict[seconditem]
    #         if 'classement' in s and seconditem != item and dict((x, i[x]) for x in ('d0', 'd1', 'd2', 'd3')) == dict(
    #                 (y, s[y]) for y in ('d0', 'd1', 'd2', 'd3')):
    #             i['classement'] = s['classement']
    #     if not 'classement' in i:
    #         i['classement'] = c
    #     c += 1
    #
    # Calcule le classement en tenant compte des égalités et des places perdues de ce fait
    # pour attribuer à chacun sa place réelle dans une clef 'classement'
    c = 1
    for i in equipe_ordo:
        for s in equipe_ordo:
            if 'classement' in s and s != i and dict((x, i[x]) for x in ('d0', 'd1', 'd2', 'd3')) == dict(
                    (y, s[y]) for y in ('d0', 'd1', 'd2', 'd3')):
                i['classement'] = s['classement']
        if 'classement' not in i:
            i['classement'] = c
        c += 1
    # joueur_dict = {"joueurs": equipe_odict}
    return equipe_ordo


def matchs_awrx(xml):
    infos = xml.xpath("/tournament/round")
    rid = 0
    matchs_dict = {}
    earj[:] = []
    eq_num_liste = []
    for ronde in infos:
        mid = 0
        if int(ronde.get('numero')) > 0:
            rid += 1
            rinfos = {}
            for match in ronde:
                mid += 1
                if int(match.get('victories')) == 1:
                    resultat = True
                elif int(match.get('defeats')) == 1:
                    resultat = False
                else:
                    resultat = None
                e1 = match.get('team1')
                e2 = match.get('team2')
                eq_num_liste.extend([e1, e2])
                minfos = {"equipe1": e1,
                          "equipe2": e2,
                          "resultat": resultat}
                rinfos.update({"match{}".format(mid): minfos})
            # rinfos.update({"nbr_matchs": mid})
            matchs_dict.update({"ronde{}".format(rid): rinfos})
    # matchs_dict.update({'nbr_rondes': rid})
    earj.extend(list(set(eq_num_liste)))
    print('#############################################################')
    print(earj)
    print('#############################################################')
    return matchs_dict


def maj_ou_creation_organisation(request, post_dict, session_dict, db):
    # Si on trouve un dictionnaire correspondant au nom passé dans la session, c'est que le site existe déjà dans la DB
    dbo_id = ''
    if session_dict in request.session.keys():
        dict_update = {}
        dbo_id = post_dict['id']
        # Vérifie si il y a des infos différentes dans le POST par rapport au dictionnaire de session
        for k, v in request.session[session_dict].items():
            # quand la clé existe, n'est pas 'id', que sa valeur dans POST n'est pas nulle et est différente de l'originale
            if k in post_dict and k != 'id' and post_dict[k] and post_dict[k] != v:
                dict_update[k] = post_dict[k]
        # si dict_update n'est pas vide, nous avons des valeurs à mettre à jour
        if dict_update:
            # on ne peut pas utiliser .update(**orga_update) sinon on bypass les méthodes (ex: logos_nom_unique)
            dbo = db.objects.get(pk=dbo_id)
            for k in dict_update:
                dbo.__dict__[k] = dict_update[k]
                dbo.save()
    # SINON, nous allons créer un nouvel objet dans la DB avec les valeurs du formulaires
    else:
        if post_dict:
            if 'pays' in post_dict:
                post_dict['pays'] = Pays.objects.get(pk=post_dict['pays'])
            newdbo = db.objects.create(**post_dict)
            newdbo.save()
            dbo_id = newdbo.id
    return dbo_id


def construct_orga(type, numid):
    infos = {}
    sub = []
    if type == 'o':
        principal = Site.objects.get(pk=numid)
        drapeau = principal.pays.drapeau
        tournois = Tournoi.objects.filter(site=principal)
        for t in tournois:
            sub.append(t.co_organisateur)
    else:
        principal = CommunauteJoueurs.objects.get(pk=numid)
        drapeau = ""
        tournois = Tournoi.objects.filter(co_organisateur=principal)
        for t in tournois:
            sub.append(t.site)
    infos['principal'] = principal
    infos['drapeau'] = drapeau
    print(list(set(sub)))
    infos['sub'] = list(set(sub))
    return infos


def derniers_t(type, nombre, **obj):
    if type == "general":
        derniers_t = Tournoi.objects.all().order_by('timestamp').reverse()[:nombre]
    elif type == "orga":
        derniers_t = Tournoi.objects.filter(**obj).order_by('timestamp').reverse()[:nombre]
    tournois = []
    for t in derniers_t:
        t_dict = {}
        t_dict['ordre'] = t.timestamp
        topeqj = t.equipej_set.all().order_by('classement')[:3]
        infost = {}
        infost['id'] = t.id
        infost['pays'] = t.site.pays
        infost['date'] = datetime(t.annee, t.mois, t.jour).date()
        infost['site'] = t.site
        if t.co_organisateur:
            infost['coorg'] = t.co_organisateur
        infost['type'] = t.type.nom
        infost['format'] = t.format.nom
        infost['carte'] = t.carte
        infost['rotation'] = t.carte_pivotee
        infost['nbr_participants'] = len(t.equipej_set.all())
        if t.krosmasterset:
            infost['krosmasterset'] = t.krosmasterset
        t_dict['infos'] = infost
        rondes = Ronde.objects.filter(tournoi=t)
        nbr_rondes = len(rondes)
        max_pts = nbr_rondes * 3
        classement = []
        for eqj in topeqj:
            classement.append(objet_eqj(eqj, max_pts))
        t_dict['classement'] = classement
        tournois.append(t_dict)
    return tournois


def liste_t(clef, valeur):
    tournois = []
    if clef == 'joueur':
        eqjs = Joueur.objects.get(pk=valeur).equipe_joueur.all()
        for eqj in eqjs:
            t = {'place': eqj.classement}
            t['pts'] = eqj.d0
            t['tournoi'] = eqj.tournoi
            t['timestamp'] = eqj.tournoi.timestamp
            tournois.append(t)
        print(tournois)
    elif clef == 'date':
        if len(valeur) == 4:
            date = {'annee': int(valeur)}
        elif len(valeur) == 7:
            date = {'annee': int(valeur[:4]), 'mois': int(valeur[5:7])}
        else:
            date = {'annee': int(valeur[:4]), 'mois': int(valeur[5:7]), 'jour': int(valeur[8:10])}
        # TODO a checker, pas sur du tout ;-)
        liste = Tournoi.objects.filter(**date).order_by('timestamp').reverse()
        for obj in liste:
            t = {'tournoi': obj}
            t['timestamp'] = obj.timestamp
            tournois.append(t)
    for t in tournois:
        obj = t['tournoi']
        t['id'] = obj.id
        t['pays'] = obj.site.pays
        t['date'] = datetime(obj.annee, obj.mois, obj.jour).date()
        t['krosmasterset'] = obj.krosmasterset
        t['site'] = obj.site
        if obj.co_organisateur:
            t['coorg'] = obj.co_organisateur
        t['type'] = obj.type.nom
        t['format'] = obj.format.nom
        t['carte'] = obj.carte
        t['rotation'] = obj.carte_pivotee
        t['nbr_participants'] = len(obj.equipej_set.all())
        rondes = Ronde.objects.filter(tournoi=obj)
        nbr_rondes = len(rondes)
        max_pts = nbr_rondes * 3
        t['pts_pct'] = (t['pts'] / max_pts) * 100
    tournois.sort(key=itemgetter('timestamp'), reverse=True)
    print(tournois)
    return tournois


def liste_t2(pays, joueur, **dict):
    tournois = []
    t_set = []
    if pays:
        s_set = Site.objects.filter(pays=pays)
        t_set= Tournoi.objects.filter(site__in=s_set)
    if joueur:
        t_set = Tournoi.objects.filter(equipej__joueurs=joueur)
    if not pays and not joueur:
        t_set = Tournoi.objects.all()
    for t in t_set.filter(**dict):
        t_dict = {}
        t_dict['ordre'] = t.timestamp
        infost = {}
        infost['id'] = t.id
        infost['pays'] = t.site.pays
        infost['date'] = datetime(t.annee, t.mois, t.jour).date()
        infost['site'] = t.site
        if t.co_organisateur:
            infost['coorg'] = t.co_organisateur
        infost['type'] = t.type
        infost['format'] = t.format.nom
        infost['carte'] = t.carte
        infost['rotation'] = t.carte_pivotee
        infost['nbr_participants'] = len(t.equipej_set.all())
        t_dict['infos'] = infost
        tournois.append(t_dict)
    return tournois


def objet_eqj(eqj, max_pts):
    equipe = {}
    equipe['place'] = eqj.classement
    equipe['nom'] = eqj.joueur_affiche()
    equipe['idj'] = eqj.joueurs.all()[0].id
    equipe['d0'] = eqj.d0
    equipe['pts_pct'] = (eqj.d0 / max_pts) * 100
    equipe['d1'] = eqj.d1
    equipe['d2'] = eqj.d2
    equipe['d3'] = eqj.d3
    if eqj.equipe_krosmasters is not None:
        # On ajoute au dictionnaire un dictionnaire de valeur contruit par la fonction objet_eqk
        equipe.update(objet_eqk(eqj.equipe_krosmasters))
    return equipe



def objet_eqk(eqk):
    equipe = {'init': eqk.total_init,
              'nbrfig': eqk.total_krosmaster,
              'nomeqk': eqk.identifiant,
              'ideqk': eqk.id,
              'k': liste_k(nomeqk=eqk.identifiant, nbrfig=eqk.total_krosmaster)
              }
    return equipe


def liste_k(nomeqk, nbrfig):
    figs = [nomeqk[i:i + 4] for i in range(0, nbrfig * 4, 4)]
    eqk = []
    for fig in figs:
        fig_infos = []
        k = Krosmaster.objects.get(numero=fig)
        # désormais on fournit également la lettre de façon apouvoir créer un lien vers la bonne version de fig.
        # les 3 chiffres seront déduits dans les templates grace à => |slice:":3"
        fig_infos.append(fig[0:4])
        fig_infos.append(k.nom)
        fig_infos.append(k.boss)
        eqk.append(fig_infos)
    return eqk


def get_krosmasters(request):
    # TODO contrôler la source de la requête ou si l'utilisateur est authentifié
    # if not request.user.is_authenticated():
    #     return Krosmaster.objects.none()
    q = request.GET.get('term', '')
    k = request.GET.get('ks_id', '')
    if k:
        ks = KrosmasterSet.objects.get(id=k)
        krosmasters = Krosmaster.objects.filter(nom__icontains=q, krosmasterset__nom=ks.nom, krosmasterset__max_etoiles__lte=ks.max_etoiles)
    else:  # On gère les demande sans KrosmasterSet défini (page d'accueil par exemple)
        krosmasters = Krosmaster.objects.filter(nom__icontains=q).exclude(krosmasterset__nom="indéfini").order_by('nom')
    results = []
    for krosmaster in krosmasters:
        krosmaster_json = {}
        krosmaster_json['label'] = krosmaster.nom if k else krosmaster.nom + ' - ' + krosmaster.krosmasterset.__str__()
        krosmaster_json['numero'] = krosmaster.numero
        krosmaster_json['niveau'] = krosmaster.niveau
        krosmaster_json['initiative'] = krosmaster.initiative
        krosmaster_json['boss'] = krosmaster.boss
        krosmaster_json['nombre_max'] = krosmaster.nombre_max
        results.append(krosmaster_json)
    data = json.dumps(results)
    mimetype = 'application/json'
    print(results)
    return HttpResponse(data, mimetype)


def get_joueurs(request):
    # TODO contrôler la source de la requête ou si l'utilisateur est authentifié
    # if not request.user.is_authenticated():
    #     return Krosmaster.objects.none()
    q = request.GET.get('term', '')
    joueurs = Joueur.objects.filter(Q(pseudo__icontains=q))
    results = []
    for joueur in joueurs:
        joueur_json = {}
        joueur_json['label'] = joueur.__str__() + ' (' + joueur.displayname + ')'
        joueur_json['numero'] = joueur.id
        results.append(joueur_json)
    data = json.dumps(results)
    mimetype = 'application/json'
    print(results)
    return HttpResponse(data, mimetype)


def get_sites(request):
    # TODO contrôler la source de la requête ou si l'utilisateur est authentifié
    # Don't forget to filter out results depending on the visitor !
    # if not request.user.is_authenticated():
    #     return Site.objects.none()
    q = request.GET.get('term', '')
    sites = Site.objects.filter(nom__icontains=q)
    results = []
    for site in sites:
        site_json = {}
        site_json['id'] = site.id
        site_json['label'] = site.nom
        site_json['adresse'] = site.adresse
        site_json['code_postal'] = site.code_postal
        site_json['ville'] = site.ville
        site_json['etat'] = site.etat
        site_json['pays'] = site.pays.nom
        site_json['pays_id'] = site.pays.id
        site_json['courriel'] = site.courriel
        site_json['web'] = site.web
        site_json['facebook'] = site.facebook
        site_json['twitter'] = site.twitter
        site_json['logo'] = str(site.logo)  # on veut juste le Path relatif pas l'image
        results.append(site_json)
    data = json.dumps(results)
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)


def get_communautes(request):
    # TODO contrôler la source de la requête ou si l'utilisateur est authentifié
    # Don't forget to filter out results depending on the visitor !
    # if not request.user.is_authenticated():
    #     return Site.objects.none()
    q = request.GET.get('term', '')
    comus = CommunauteJoueurs.objects.filter(nom__icontains=q)
    results = []
    for comu in comus:
        comu_json = {}
        comu_json['id'] = comu.id
        comu_json['label'] = comu.nom
        comu_json['courriel'] = comu.courriel
        comu_json['web'] = comu.web
        comu_json['facebook'] = comu.facebook
        comu_json['twitter'] = comu.twitter
        comu_json['logo'] = str(comu.logo)  # on veut juste le Path relatif pas l'image
        results.append(comu_json)
    data = json.dumps(results)
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)


def get_cartes(request):
    # TODO contrôler la source de la requête ou si l'utilisateur est authentifié
    # if not request.user.is_authenticated():
    #     return Krosmaster.objects.none()
    q = request.GET.get('term', '')
    cartes = Carte.objects.filter(Q(nom__icontains=q) | Q(surnom__icontains=q))
    results = []
    for carte in cartes:
        carte_json = {'label': carte.nom + ' (' + carte.surnom + ')',
                      'numero': carte.id}
        results.append(carte_json)
    data = json.dumps(results)
    mimetype = 'application/json'
    print(results)
    return HttpResponse(data, mimetype)


def get_mois(request):
    q = request.GET.get('term', '')
    rq = request.META.get('HTTP_REFERER')
    # si on trouve /en/ dans l'URL, on paramètre la locale en conséquence
    pattern = re.compile("/en/")
    if pattern.search(rq):
        locale.setlocale(locale.LC_ALL, "en_US.utf8")
        all = 'Month'
    else:
        locale.setlocale(locale.LC_ALL, "fr_BE.utf8")
        all = 'Mois'
    mois = Tournoi.objects.filter(annee=q).values_list('mois', flat=True).distinct()
    mois_dispo = {}
    for moi in mois:
        mois_dispo[moi] = calendar.month_name[moi]
    mois_dispo[0] = all # Il faut un champs non sélectionné pour toute l'année
    sorted(mois_dispo)
    data = json.dumps(mois_dispo)
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)

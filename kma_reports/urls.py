from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.views.generic import TemplateView
from rest_framework.urlpatterns import format_suffix_patterns
from krosympas.settings import env

from . import views

urlpatterns = [
                  url(r'^auth_login$', views.auth_login, name='auth_login'),
                  url(r'^auth_logout$', views.auth_logout, name='auth_logout'),
                  url(r'^auth_profile$', views.auth_profile, name='auth_profile'),
                  url(r'^accueil$', views.accueil, name='accueil'),
                  url(r'^infos$', TemplateView.as_view(template_name='infos.html'), name='infos'),
                  url(r'^soutien$', views.soutien, name='soutien'),
                  # url(r'^\d{4}$', views.annee, name='annee'),
                  # url(r'^\d{4}\/\d{2}$', views.mois, name='mois'),
                  # url(r'^\d{4}\/\d{2}\/\d{2}$', views.jour, name='jour'),
                  url(r'^upload_tournoi$', views.upload_tournoi, name='upload_tournoi'),
                  url(r'^encode_tournoi$', views.encode_tournoi, name='encode_tournoi'),
                  url(r'^encode_pseudos$', views.encode_pseudos, name='encode_pseudos'),
                  url(r'^encode_equipes$', views.encode_equipes, name='encode_equipes'),
                  url(r'^encode_fin$', views.encode_fin, name='encode_fin'),
                  url(r'^affichage_tournoi$', views.affichage_tournoi, name='affichage_tournoi'),
                  url(r'^affichage_orga$', views.affichage_orga, name='affichage_orga'),
                  url(r'^affichage_joueur$', views.affichage_joueur, name='affichage_joueur'),
                  url(r'^affichage_equipe$', views.affichage_equipe, name='affichage_equipe'),
                  url(r'^affichage_krosmaster$', views.affichage_krosmaster, name='affichage_krosmaster'),
                  url(r'^affichage_carte$', views.affichage_carte, name='affichage_carte'),
                  url(r'^liste_tournois$', views.liste_tournois, name='liste_tournois'),
                  url(r'^top_equipes$', views.top_equipes, name='top_equipes'),
                  url(r'^api/get_krosmasters/$', views.get_krosmasters, name='get_krosmasters'),
                  url(r'^api/get_joueurs/$', views.get_joueurs, name='get_joueurs'),
                  url(r'^api/get_sites/$', views.get_sites, name='get_sites'),
                  url(r'^api/get_communautes/$', views.get_communautes, name='get_communautes'),
                  url(r'^api/get_cartes/$', views.get_cartes, name='get_cartes'),
                  url(r'^api/get_mois/$', views.get_mois, name='get_mois'),
                  url(r'^rosetta/', include('rosetta.urls')),
                  url(r'^.*$', views.accueil, name='accueil'),
              ] \
              + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) \
              + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if env.DEV_ENV:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

urlpatterns = format_suffix_patterns(urlpatterns)
# Gestion des URLs
#
# Date => Site
# -------------
# 2016                    Page avec liste des mois où il y a des tournois (éventuellement TL, QT, Drapeaux, etc. en +)
#                   ^\d{4}$
# 2016/01                 Page avec liste des jours où il y a des tournois (éventuellement TL, QT, Drapeaux, etc. en +)
#                   ^\d{4}\/\d{2}$
# 2016/01/01              liste des lieux de tournois par Nationaux puis Pays et Site
#                   ^\d{4}\/\d{2}\/\d{2}$
# 2016/01/01/site         renvoit vers le 2016/01/01/site/0 si il n'y qu'un tournoi pour ce site à cette date (défaut)
#                         ou une page avec les différents tournois sur ce site à la date demandée
#                         ou une page listant si possible les 2 derniers tournoi et les 2 suivants sur ce site
#                         + les autres sites où il y a eu des tournois à cette date, si pas de tournoi pour le site.
#                   ^\d{4}\/\d{2}\/\d{2}\/$
# 2016/01/01/aux3d/0      Permet de retrouver les tournois avec possibilités de plusieurs tournois par date/lieu
#                         0 est le tournoi par défaut, 1,2, ... en fonction de l'ordre d'encodage sur base de l'ID
#                   ^\d{4}\/\d{2}\/\d{2}\/.*\/0$
#                   ^\d{4}\/\d{2}\/\d{2}\/.*\/\d{1}$    si liste 1 seulle position comme si 0
#                                                       si liste > 1, Tournoi à la Nième position de la liste d'ID
#                                                       si liste vide voir 2016/01/01/site
#
# Nom
# ----
# nom                     Cherche si il existe un pseudo, un site, un national ou un pays et retourne
#                         la page du nom unique OU la liste des occurences,
# prenom/nx               Cherche si il existe un prenom/initiales et retourne la page OU la liste des occurences

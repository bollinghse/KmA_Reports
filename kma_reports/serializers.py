from rest_framework import serializers
from kma_reports.models import Krosmaster, EquipeK


class KrosmasterSerializer(serializers.ModelSerializer):
    filter_fields = ('label', 'niveau',)
    label = serializers.CharField(source='nom')
    value = serializers.CharField(source='nom')
    class Meta:
        model = Krosmaster
        fields = ('label', 'value', 'niveau', 'initiative', 'boss', 'nombre_max', 'autorise', 'numero',)

class EquipeKSerializer(serializers.ModelSerializer):
    krosmaster = KrosmasterSerializer()
    class Meta:
        model = EquipeK
        fields = ('identifiant', 'total_init', 'total_krosmaster', 'boss', 'krosmasters',)


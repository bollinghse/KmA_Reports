import os
import uuid

from django.conf import settings
from django.db import models
from django.core.files.storage import FileSystemStorage
from django.contrib.auth.models import User
from django.utils.text import slugify

def carte_upload(instance, filename):
    ext = filename.split('.')[-1]
    name = filename.split('.')[0]
    filename = "%s.%s" % (slugify(name), ext)
    return os.path.join('cartes', filename)


class Carte(models.Model):
    nom = models.CharField(max_length=40, unique=True)
    surnom = models.CharField(max_length=40, blank=True, null=True)
    image = models.ImageField(upload_to=carte_upload)

    def __str__(self):
        if self.surnom:
            return u'%s - (%s)' % (self.nom, self.surnom)
        else:
            return u'%s' % self.nom


class Pays(models.Model):
    nom = models.CharField(max_length=30, unique=True)
    drapeau = models.ImageField(upload_to="pays")
    class Meta:
        verbose_name_plural = "Pays"

    def __str__(self):
        return u'%s' % self.nom


class Format(models.Model):
    nom = models.CharField(max_length=30, unique=True)

    def __str__(self):
        return u'%s' % self.nom


class Type(models.Model):
    nom = models.CharField(max_length=30, unique=True)

    def __str__(self):
        return u'%s' % self.nom


class KrosmasterSet(models.Model):
    nom = models.CharField(max_length=30)
    max_etoiles = models.PositiveSmallIntegerField(blank=True)

    def __str__(self):
        if self.max_etoiles:
            return u'%s (%s*)' % (self.nom, self.max_etoiles)
        else:
            return u'%s' % self.nom


class Krosmaster(models.Model):
    nom = models.CharField(max_length=30)
    niveau = models.PositiveSmallIntegerField()
    initiative = models.PositiveSmallIntegerField()
    boss = models.BooleanField()
    nombre_max = models.PositiveSmallIntegerField()
    autorise = models.BooleanField(default=True)
    saison = models.BooleanField(default=False)
    eternel = models.BooleanField(default=False)
    etoiles = models.PositiveSmallIntegerField(default=0)
    krosmasterset = models.ForeignKey(KrosmasterSet, blank=True, null=True)
    numero = models.CharField(max_length=4, unique=True)  # lien images, suis numérotation de de KrosFinder
    class Meta:
        verbose_name = 'Krosmaster'

    def __str__(self):
        return u'%s' % self.nom


class EquipeK(models.Model):
    # NB l'identifiant est composé de 8 groupes de 3 chiffres et 1 lettre  a correspondant
    # au numéro du krosmaster cela permet de connaitre le nombre et la place de chaque krosmaster
    # dans un identifiant unique. Les places vides de l'équipes sont représentées par 0000
    # NB2 si des profils de krosmaster évoluent (+boss) il faudra les dupliquer avec 1 autre lettre
    # afin de conserver l'unicité de l'identifiant tout en refletant la réalité d'un moment b, c, etc.
    identifiant = models.CharField(max_length=32, unique=True)
    total_init = models.PositiveSmallIntegerField()
    total_krosmaster = models.PositiveSmallIntegerField()
    boss = models.BooleanField(default=False)
    krosmasters = models.ManyToManyField(Krosmaster, related_name="equipe_krosmaster", through='EquipeK_Krosmaster')
    class Meta:
        verbose_name = 'Equipe de Krosmasters'
        verbose_name_plural = "Equipes de Krosmasters"

    def __str__(self):
        return u'%s' % self.identifiant


# TODO check suppression en cascade pour virer la table de liaison mais pas les krosmaster
class EquipeK_Krosmaster(models.Model):
    equipek = models.ForeignKey(EquipeK)
    krosmaster = models.ForeignKey(Krosmaster)
    nombrek = models.PositiveSmallIntegerField()


class Joueur(models.Model):
    numero_de_carte = models.CharField(max_length=20, blank=True, unique=True, default=None)
    pseudo = models.CharField(max_length=30, blank=True, null=True)
    displayname = models.CharField(max_length=40, blank=True, null=True)
    prenom = models.CharField(max_length=40, blank=True)
    nom = models.CharField(max_length=40, blank=True)

    def __str__(self):
        if self.pseudo:
            return u'%s' % self.pseudo
        else:
            return u'%s' % self.displayname

def logos_nom_unique(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('logos/', filename)


class InternetData(models.Model):
    web = models.URLField(blank=True, null=True)
    twitter = models.URLField(blank=True, null=True)
    facebook = models.URLField(blank=True, null=True)
    courriel = models.EmailField(blank=True, null=True)
    logo = models.ImageField(upload_to=logos_nom_unique, blank=True, null=True)


class CommunauteJoueurs(InternetData):
    nom = models.CharField(max_length=30, unique=True)
    class Meta:
        verbose_name = 'Communauté de Joueurs'
        verbose_name_plural = "Communautés de Joueurs"

    def __str__(self):
        return u'%s' % self.nom


class Site(InternetData):
    nom = models.CharField(max_length=40, unique=True)
    adresse = models.CharField(max_length=50, blank=True)
    code_postal = models.CharField(max_length=10, blank=True)
    ville = models.CharField(max_length=40, blank=True)
    etat = models.CharField(max_length=40, blank=True, null=True)
    pays = models.ForeignKey(Pays)

    def __str__(self):
        return u'%s' % self.nom


class Tournoi(models.Model):
    annee = models.PositiveSmallIntegerField()
    mois = models.PositiveSmallIntegerField()
    jour = models.PositiveSmallIntegerField()
    timestamp = models.IntegerField()
    site = models.ForeignKey(Site)
    co_organisateur = models.ForeignKey(CommunauteJoueurs, blank=True, null=True)
    carte = models.ForeignKey(Carte)
    carte_pivotee = models.BooleanField(default=False)
    format = models.ForeignKey(Format, blank=True, null=True)
    type = models.ForeignKey(Type, blank=True, null=True)
    krosmasterset = models.ForeignKey(KrosmasterSet, blank=True, null=True)
    homologue = models.BooleanField(default=True)
    encodeur = models.ForeignKey(User, blank=True, null=True)

    def __str__(self):
        return u'%s - %s/%02d/%02d' % (self.site, self.annee, self.mois, self.jour)

    def date(self):
        return u'%s/%02d/%02d' % (self.annee, self.mois, self.jour)


# Les équipes n'existent que dans le cadre d'un tournoi ici
class EquipeJ(models.Model):
    numero = models.PositiveIntegerField()
    d0 = models.PositiveSmallIntegerField()
    d1 = models.DecimalField(max_digits=20, decimal_places=17)
    d2 = models.DecimalField(max_digits=20, decimal_places=17)
    d3 = models.DecimalField(max_digits=20, decimal_places=17)
    classement = models.PositiveSmallIntegerField(blank=True, null=True)
    tournoi = models.ForeignKey(Tournoi, blank=True, null=True)
    joueurs = models.ManyToManyField(Joueur, related_name="equipe_joueur", blank=True)
    equipe_krosmasters = models.ForeignKey(EquipeK, blank=True, null=True)
    class Meta:
        verbose_name = 'Equipe de Joueurs'
        verbose_name_plural = "Equipes de Joueurs"

    def joueur_affiche(self):
        if self.joueurs.all()[0].pseudo:
            nom = self.joueurs.all()[0].pseudo
        else:
            nom = self.joueurs.all()[0].displayname
        return nom

    def __str__(self):
        return u'%s' % (self.id)


# Les rondes sont définies pour reflêter la réalité et au cas ou on déciderait de leur attribuer des infos
# celles-ci seraient alors communes à tous les matchs d'une même ronde !
class Ronde(models.Model):
    numero = models.PositiveSmallIntegerField("Ronde numéro",)
    tournoi = models.ForeignKey(Tournoi)

    def __str__(self):
        return u'T%sR%s' % (self.tournoi.id, self.numero)


class Match(models.Model):
    numero = models.PositiveSmallIntegerField("Match numéro",)
    equipe1 = models.ForeignKey(EquipeJ, related_name='eqp1')
    equipe2 = models.ForeignKey(EquipeJ, related_name='eqp2', blank=True, null=True)
    resultat = models.NullBooleanField() # Résultat peut-être Gagné(1), Perdu(0), Egalité(null)
    ronde = models.ForeignKey(Ronde)

    def __str__(self):
        return u'T%sR%sM%03d' % (self.ronde.tournoi.id, self.ronde.numero, self.numero)

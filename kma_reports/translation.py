from modeltranslation.decorators import register
from modeltranslation.translator import TranslationOptions
from kma_reports.models import Carte, Format, Krosmaster, Type


@register(Krosmaster)
class CarteTranslationOptions(TranslationOptions):
    fields = ('nom',)

@register(Carte)
class CarteTranslationOptions(TranslationOptions):
    fields = ('nom', 'surnom',)


@register(Format)
class CarteTranslationOptions(TranslationOptions):
    fields = ('nom',)


@register(Type)
class CarteTranslationOptions(TranslationOptions):
    fields = ('nom',)
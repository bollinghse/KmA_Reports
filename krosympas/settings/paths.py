import os

# BASE_DIR est l'endroit où se trouve manage.py
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

# PROJECT_ROOT est l'endroit où se trouvent les settings
PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
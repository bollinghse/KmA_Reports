from krosympas.settings import env

DEBUG = env.DEV_ENV
#TEMPLATE_DEBUG = DEBUG

if env.DEV_ENV:
    def show_toolbar(request):
        return True
    DEBUG_TOOLBAR_CONFIG = {
        "SHOW_TOOLBAR_CALLBACK" : show_toolbar,
    }
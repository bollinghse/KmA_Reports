import os

from krosympas.settings import paths

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'UTC'

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'fr-fr'

#TRANSLATABLE_MODEL_MODULES = ["kma_reports.models",]
MODELTRANSLATION_DEFAULT_LANGUAGE = 'fr'
MODELTRANSLATION_AUTO_POPULATE = True
MODELTRANSLATION_LANGUAGES = ('fr', 'en')
#MODELTRANSLATION_LANGUAGES = ('fr', 'en', 'de', 'pt')

# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

from django.utils.translation import ugettext_lazy as _
LANGUAGES = (
    ('en', _('English')),
    ('fr', _('French')),
    # ('de', _('German')),
    # ('pt', _('Portuguese')),
    # ('it', _('Italian')),
    # ('es', _('Spanish')),
)

LOCALE_PATHS = (
    os.path.join(paths.BASE_DIR, 'locale'),
)
from krosympas.settings import env

LOGIN_URL = 'auth_login'

INSTALLED_APPS = (
    'modeltranslation',  # Nécessaire pour profiter de l'intégration dans admin
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    #'django.contrib.sites', # django 1.6.2+
    'django.contrib.humanize',
    # 'django_nyt',
    # 'mptt',
    'sekizai',
    # 'sorl.thumbnail',
    'bootstrap3',
    'rest_framework',
    'email_obfuscator',
    'columns',
    'rosetta',
    'kma_reports',
)

if env.DEV_ENV:
    INSTALLED_APPS += (
        'debug_toolbar',
    )

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ]
}